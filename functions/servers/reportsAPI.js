class ReportsAPI{
    constructor(server,admin){
        const app = server
        const firebase = admin

        app.post('/getReports', (request, response) => {
            console.log("/getReports")
            // console.log(request.body)
            // customer,services,beatician,date range,
            if (!request.body.uid)
                throw new Error('data/incorrect-parameters')
            // console.log(request.originalUrl)
            // fix the error here
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            let ref = ""
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    // console.log(res.user_id,request.body.uid)
                    if (res.user_id !== request.body.uid)
                        throw new Error('auth/wrong-password')
                    let ref = firebase.firestore().collection('TRANSACTIONS')
                    if(request.body.operatorId)
                        ref = ref.where("operatorId","==",request.body.operatorId)
                    if(request.body.customerId)
                        ref = ref.where("customerId", "==", request.body.customerId)
                    if(request.body.date){
                        if(request.body.date.year)
                            ref = ref.where("date.year", "==", request.body.date.year)
                        if (request.body.date.month)
                            ref = ref.where("date.month", "==", request.body.date.month)
                        if (request.body.date.year)
                            ref = ref.where("date.day", "==", request.body.date.day)
                    }
                    return ref.get()
                }).then(snapshot => {
                    // console.log(snapshot.size)
                    const docs = []
                    snapshot.docs.map(document => {
                        // console.log(document)
                        return docs.push(document.data())
                    });
                    // console.log(docs)
                    return Promise.all(docs)
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        transactions: res
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().split('\n')[0] === 'Error: auth/wrong-password') {
                        // console.log('user id wrong')
                        return response.status(200).send({
                            code: "auth/wrong-password",
                            message: "The password is invalid or the user does not have a password."
                        })
                    } else if (err.toString().split('\n')[0] === 'Error: data/incorrect-parameters') {
                        // console.log('user id wrong')
                        return response.status(200).send({
                            code: "data/incorrect-parameters",
                            message: "Parameters supplied are insufficient to complete the process."
                        })
                    }
                    return response.status(200).send(err)
                })
        });
    }
}

module.exports = ReportsAPI