
class OpenAPI{
    constructor(server,firebase){
        const app = server
        const admin = firebase

        app.post('/getUid',(request,response) => {
            console.log('/getUid')
            //console.log(request)
            // console.log(request.body)
            return firebase.auth().getUser(request.body.uid)
            .then(user => {
                // console.log(user.toJSON().uid)
                return admin.firestore().collection('USERS').doc(user.toJSON().uid).get()
            })
            .then(user => {
                return response.status(200).send(user.data())
            }).catch(err => {
                // console.log(err)
                return response.status(200).send(err)
            })
            
        });

        app.post('/createAppointment',(request,response) => {
            console.log('/createAppointment')
            const phoneNumber = request.body.phoneNumber
            const displayName = request.body.displayName
            const timestamp = request.body.timestamp
            const time = request.body.time
            const date = request.body.date
            const gender = request.body.gender
            const branch = request.body.branch

            if(!phoneNumber || !displayName || !timestamp || !time || !date || !gender || !branch)
                throw new Error('data/incorrect-parameters')

            const data = {
                uid : phoneNumber,
                displayName : displayName,
                branch : branch,
                timestamp : timestamp,
                serverTimestamp : new Date().toUTCString(),
                appointment : {
                    date : date,
                    time : time
                },
                gender : gender
            }
            // console.log(request)
            const ref = admin.firestore().collection('APPOINTMENTS').doc()
            data.id = ref.id
            return ref.set(data)
            .then(res => {
                return response.status(200).send({"status" : true})
            }).catch(err => {
                // console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        })

        app.post('/getAppointments',(request,response) => {
            console.log('/getAppointments')
            // const time = request.body.time
            let date = request.body.date
            // console.log(request.body)
            if(!date)
                throw new Error('data/incorrect-parameters')

            // date = "2020-01-06"
            // console.log(request)
            let ref = admin.firestore().collection('APPOINTMENTS').where("appointment.date","==",date)
            // if(time)
            //     ref = ref.where("appointment.time","==",time)
            return ref.get()
            .then(docList => {
                const promises = []
                docList.docs.forEach(doc => {
                    promises.push(doc.data())
                })
                return Promise.all(promises)
            })
            .then(res => {
                console.log(res)
                return response.status(200).send({appointments : res})
            }).catch(err => {
                // console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        })
    }

}

module.exports = OpenAPI