class TransactionAPI{
    constructor(server,firebase){
        const app = server
        const admin = firebase

        app.post('/securePayment',(request,response)=>{
            console.log("/securePayment")
            // console.log(request.body)
            if(!request.body.username || !request.body.uid || !request.body.date || !request.body.timestamp || !request.body.paymentType)
                throw new Error('data/incorrect-parameters')
            if(!request.body.discount)
                request.body.discount = 0
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            // const collectionName = 'TRANSACTIONS/'+request.body.uid+'/'+request.body.date.year+'/'+request.body.date.month+'-'+request.body.date.day+'/'+request.body.username
            const collectionName = 'TRANSACTIONS'
            var documentId = ""
            var totalTransactions = 0
            var presentTransactions = 0
            return admin.auth().verifyIdToken(token)
            .then(res => {
                //console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                const url = request.body.username
                return firebase.database().ref(url).once('value')
            }).then(res => {
                const data = res.val()
                if(!data)
                    throw new Error('data/wrong-information')
                if(!data.items)
                    throw new Error('data/wrong-information')
                //console.log(data)
                let cost = 0
                for(let i in data.items){
                    // console.log(i)
                    cost += data.items[i].cost
                }
                data.paid = cost-parseInt(request.body.discount)
                // if(cost !== data.total)
                //     throw new Error('data/wrong-information')
                return data
            })
            .then(res => {
                // res = res.val()
                // console.log(res)
                const items = []
                for(let i in res.items){
                    items.push(res.items[i])
                }
                const ref = firebase.firestore().collection(collectionName).doc()
                documentId = ref.id
                return ref.set({
                    items : items,
                    total : res.total,
                    payment : res.total-request.body.discount,
                    date : request.body.date,
                    id: documentId,
                    timestamp : request.body.timestamp,
                    customerId : request.body.username,
                    operatorId : request.body.uid,
                    discount : request.body.discount,
                    paymentType: request.body.paymentType,
                    serverTimestamp: new Date().toUTCString()
                },{merge:true})
            }).then(res => {
                return firebase.database().ref('METADATA/TOTAL TRANSACTIONS').once('value')
            }).then(res => {
                const val = res.val() ? parseInt(res.val())+1 : 1
                totalTransactions = val
                return firebase.database().ref('METADATA/TOTAL TRANSACTIONS').set(val)
            }).then(res => {
                return firebase.database().ref('METADATA/PRESENT TRANSACTIONS').once('value')
            }).then(res => {
                const val = res.val() ? res.val() : 1
                let data = {}
                if(val.date !== new Date().toLocaleDateString())
                    data = {
                        date : new Date().toLocaleDateString(),
                        value : 1
                    }
                else
                    data = {
                        date : val.date,
                        value : parseInt(val.value)+1
                    }
                presentTransactions = data.value
                return firebase.database().ref('METADATA/PRESENT TRANSACTIONS').set(data)
            }).then(res => {
                // res = res.val()
                // console.log(res)
                const ref = firebase.firestore().collection(collectionName).doc(documentId)
                return ref.set({
                    transactionNumber : totalTransactions,
                    meta : {
                        totalTransactions : totalTransactions,
                        presentTransactions : presentTransactions
                    }
                },{merge:true})
            }).then(res => {
                return firebase.database().ref(request.body.username).set({})
            }).then(res => {
                return response.send({
                    recieptId : totalTransactions
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/wrong-information'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/wrong-information",
                        message : "data seems to be incorrect, please start the transaction again"
                    })
                }
                return response.status(500).send(err)
            })
        });

    }
}

module.exports = TransactionAPI