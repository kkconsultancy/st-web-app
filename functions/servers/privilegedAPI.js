
class PrivilegedAPI{
    constructor(server,firebase){
        const app = server
        const admin = firebase
        new (require('./reportsAPI'))(app,admin)
        new (require('./transactionAPI'))(app,admin)

        app.post('/createItem',(request,response)=>{
            console.log('/createItem')
            //console.log(request)
            if(!request.body.uid || !request.body.price || !request.body.name)
                throw new Error('data/incorrect-parameters')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')

                return firebase.firestore().collection('CATALOGUE_ITEMS').where("name","==",request.body.name)
            }).then(res => {
                const ref = firebase.firestore().collection('CATALOGUE_ITEMS').doc()
                const id = ref.id
                console.log(id,request.body)
                return ref.set({
                    name : request.body.name,
                    price : request.body.price,
                    parent : request.body.parent,
                    description : request.body.description,
                    id : id
                })
            }).then(res => {
                // console.log(res)
                return response.send({
                    data : "NEW ITEM CREATED"
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/updateItem',(request,response)=>{
            console.log('/updateItem')
            //console.log(request)
            if(!request.body.uid || !request.body.parent || !request.body.price || !request.body.name)
                throw new Error('data/incorrect-parameters')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                return firebase.firestore().collection('CATALOGUE_ITEMS').doc(request.body.id).set({
                    name : request.body.name,
                    price : request.body.price,
                    parent : request.body.parent,
                    description : request.body.description
                },{merge:true})
            }).then(res => {
                // console.log(res)
                return response.send({
                    data : "ITEM UPDATED"
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/removeItem',(request,response)=>{
            console.log('/removeItem')
            //console.log(request)
            if(!request.body.uid || !request.body.id)
                throw new Error('data/incorrect-parameters')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                return firebase.firestore().collection('CATALOGUE_ITEMS').doc(request.body.id).delete()
            }).then(res => {
                // console.log(res)
                return response.send({
                    data : "ITEM DELETED"
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/updateUser',(request,response)=>{
            console.log('/updateUser')
            // console.log(request.body)
            if(!request.body.role || !request.body.uid || !request.body.phoneNumber || !request.body.email || !request.body.displayName || !request.body.address)
                throw new Error('data/incorrect-parameters')
            
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                return firebase.firestore().collection('USERS').doc(request.body.phoneNumber).set({
                    role : request.body.role,
                    displayName : request.body.displayName.toUpperCase(),
                    address : request.body.address,
                    email : request.body.email
                },{merge:true})
            }).then(res => {
                // console.log(res)
                return response.send({
                    data : "USER UPDATED"
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/resetPassword',(request,response)=>{
            console.log('/resetPassword')
            // console.log(request.body)
            if(!request.body.uid || !request.body.username)
                throw new Error('data/incorrect-parameters')
            
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                return firebase.auth().updateUser(request.body.username,{
                    password : request.body.password ? request.body.password : 'Sasi@123'
                })
            }).then(res => {
                // console.log(res)
                return response.send({
                    data : "USER RESET COMPLETED"
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/addUser',(request,response) => {
            console.log('/addUser')
            //console.log(request)
            if(!request.body.uid || !request.body.phoneNumber || !request.body.email || !request.body.displayName || !request.body.address)
                throw new Error('data/incorrect-parameters')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            // console.log(token)
            return admin.auth().verifyIdToken(token)
            .then(res => {
                console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                return firebase.firestore().collection('USERS').doc(request.body.phoneNumber).set({
                    role : "USER",
                    uid : request.body.phoneNumber,
                    email : request.body.email,
                    displayName : request.body.displayName.toUpperCase(),
                    phoneNumber : request.body.phoneNumber,
                    parent : request.body.uid,
                    address : request.body.address
                },{merge:true})
            }).then(res => {
                // console.log(res)
                return response.send({
                    data : "NEW USER CREATED"
                })
            }).catch(err => {
                console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/addToCart',(request,response) => {
            console.log('/addToCart')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    // console.log(res.user_id,request.body.uid)
                    if(res.user_id !== request.body.uid)
                        throw new Error('auth/wrong-password')
                    return firebase.database().ref('/cart/' + request.body.username + '/' + request.body.id).set({
                        name : request.body.name,
                        quantity : request.body.quantity
                    })
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        data: "ITEM ADDED TO CART"
                    })
                }).catch(err => {
                    console.log(err)
                    if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                        // console.log('user id wrong')
                        return response.status(200).send({
                            code : "auth/wrong-password",
                            message : "The password is invalid or the user does not have a password."
                        })
                    }
                    return response.status(200).send(err)
                })
        });

        app.post('/removeFromCart', (request, response) => {
            console.log('/removeFromCart')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    // console.log(res.user_id,request.body.uid)
                    if(res.user_id !== request.body.uid)
                        throw new Error('auth/wrong-password')
                    return firebase.database().ref('/cart/' + request.body.username + '/' + request.body.id).remove()
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        data: "ITEM REMOVED FROM CART"
                    })
                }).catch(err => {
                    console.log(err)
                    if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                        // console.log('user id wrong')
                        return response.status(200).send({
                            code : "auth/wrong-password",
                            message : "The password is invalid or the user does not have a password."
                        })
                    }
                    return response.status(200).send(err)
                })
        });

        app.post('/updateInCart', (request, response) => {
            console.log('/updateInCart')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    // console.log(res.user_id,request.body.uid)
                    if(res.user_id !== request.body.uid)
                        throw new Error('auth/wrong-password')
                    return firebase.database().ref('/cart/' + request.body.username + '/' + request.body.id).update({
                        name: request.body.name,
                        quantity: request.body.quantity
                    })
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        data: "ITEM UPDATED IN CART"
                    })
                }).catch(err => {
                    console.log(err)
                    if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                        // console.log('user id wrong')
                        return response.status(200).send({
                            code : "auth/wrong-password",
                            message : "The password is invalid or the user does not have a password."
                        })
                    }
                    return response.status(200).send(err)
                })
        });

        app.post('/makeTransaction', (request, response) => {
            console.log('/makeTransaction')
            const token = request.originalUrl.toString().split('/').slice(-2)[0]
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    // console.log(res.user_id,request.body.uid)
                    if(res.user_id !== request.body.uid)
                        throw new Error('auth/wrong-password')
                    return firebase.firestore().collection('TRANSACTIONS').doc(request.body.username).set({
                        operator : request.body.uid,
                        items : request.body.items,
                        branch : request.body.branch,
                        amount : request.body.amount,
                        paid : request.body.paid,
                        discount : request.body.discount
                    },{merge:true})
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        data: "ITEM UPDATED IN CART"
                    })
                }).catch(err => {
                    console.log(err)
                    if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                        // console.log('user id wrong')
                        return response.status(200).send({
                            code : "auth/wrong-password",
                            message : "The password is invalid or the user does not have a password."
                        })
                    }
                    return response.status(200).send(err)
                })
        });
    }
}

module.exports = PrivilegedAPI