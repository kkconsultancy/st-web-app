
class ClosedAPI{
    constructor(server,firebase){
        const app = server
        const admin = firebase

        new (require('./openAPI.js'))(app,admin)

        app.post('/getServices',(request,response) => {
            
            console.log('/getServices')
            const uid = request.body.uid
            // console.log(request.originalUrl.split('/').slice(-2)[0]).0
            admin.firestore().collection('USERS').doc(uid).get()
            .then(res => {
                // console.log(res)
                if(!res.exists)
                    throw new Error('auth/no-user')

                const role = res.data().role
                if(!role)
                    throw new Error('auth/no-role')
                return admin.firestore().collection('SERVICES').doc(role).get()
            }).then(res => {
                if(!res.exists)
                    throw new Error('auth/no-user')
                // console.log(res.data())
                return response.status(200).send(res.data())
            }).catch(err => {
                // console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/no-user'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/no-user",
                        message : "no user found with the specific user id"
                    })
                }else if(err.toString().split('\n')[0] === 'Error: auth/no-role'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/no-role",
                        message : "no role found with the specific role id"
                    })
                }
                return response.status(200).send(err)
            })
        });

        app.post('/getItems',(request,response)=>{
            console.log('/getItems')
            //console.log(request)
            const uid = request.body.uid
            const token = request.body.token
            // console.log(request.body)
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== request.body.uid)
                    throw new Error('auth/wrong-password')
                return firebase.firestore().collection('CATALOGUE_ITEMS').get()
            }).then(res => {
                const result = res.docs.map(doc => {
                    return doc.data()
                });
                return response.send(result)
            }).catch(err => {
                // console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }
                return response.status(200).send(err)
            })
            
        });

        app.post('/getUsers',(request,response)=>{
            console.log('/getUsers')
            //console.log(request)
            const uid = request.body.uid
            const token = request.body.token
            if(!uid || !token)
                throw new Error('data/incorrect-parameters')
            // console.log(request.body)
            return admin.auth().verifyIdToken(token)
            .then(res => {
                // console.log(res.user_id,request.body.uid)
                if(res.user_id !== uid)
                    throw new Error('auth/wrong-password')
                return firebase.firestore().collection('USERS').get()
            }).then(res => {
                const result = res.docs.map(doc => {
                    return doc.data()
                });
                return response.send(result)
            }).catch(err => {
                // console.log(err)
                if(err.toString().split('\n')[0] === 'Error: auth/wrong-password'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "auth/wrong-password",
                        message : "The password is invalid or the user does not have a password."
                    })
                }else if(err.toString().split('\n')[0] === 'Error: data/incorrect-parameters'){
                    // console.log('user id wrong')
                    return response.status(200).send({
                        code : "data/incorrect-parameters",
                        message : "Parameters supplied are insufficient to complete the process."
                    })
                }
                return response.status(200).send(err)
            })
            
        });

    }

}

module.exports = ClosedAPI