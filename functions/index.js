const functions = require('firebase-functions');
const admin = require('firebase-admin')
const cors = require('cors')({origin:true})
const express = require('express')

admin.initializeApp(functions.config().firebase)

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const app = express()
app.use(express.json())
app.use(cors)
const openAPI = express()
const closedAPI = express()
const priviegedAPI = express()

app.use('/v1',closedAPI) 
app.use('/v1/public',openAPI)
app.use('/v1/:token',priviegedAPI)

new (require('./servers/closedAPI.js'))(closedAPI,admin)
new (require('./servers/openAPI.js'))(openAPI,admin)
new (require('./servers/privilegedAPI.js'))(priviegedAPI,admin)

exports.api = functions.https.onRequest(app)

exports.newUserListener = functions.firestore.document("USERS/{user}").onCreate((change,context) => {
    const raw = change.data()
    const data = {
        displayName: raw.displayName,
        uid : raw.uid,
        email : raw.email,
        password : "Sasi@123",
        phoneNumber : raw.phoneNumber,
        photoURL: "https://firebasestorage.googleapis.com/v0/b/sparkle-twilight.appspot.com/o/PROFILES%2Fdefault.jpg?alt=media&token=aa1ed9d3-b955-4081-921f-fef73d315d1d"
    }
    return admin.auth().createUser(data)
    .then( user=> {
        console.log(user)
        return Promise.resolve(user)
    }).catch(err => {
        throw err
    })
    
});

exports.userUpdation = functions.firestore.document("USERS/{user}").onUpdate((change,context) => {
    const raw = change.after.data()
    // console.log(raw)
    const data = {
        displayName: raw.displayName,
        email : raw.email,
        photoURL: raw.photoURL
    }
    return admin.auth().updateUser(raw.uid,data)
    .then(user => {
        console.log(user)
        return Promise.resolve(user)
    }).catch(err => {
        throw err
    })
})

// eslint-disable-next-line no-extend-native
String.prototype.hashCode = function (key = 26) {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << key) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
exports.getFirebaseConfig = functions.https.onRequest((request,response) => {
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body }
    // console.log(conditions.parameters.body)
    const firebaseConfigJSON = {
        apiKey: "AIzaSyAGx1czMI8JCK7DEOrUrqgd_sLgia_ludA",
        authDomain: "sparkle-twilight.firebaseapp.com",
        databaseURL: "https://sparkle-twilight.firebaseio.com",
        projectId: "sparkle-twilight",
        storageBucket: "sparkle-twilight.appspot.com",
        messagingSenderId: "827669200565",
        appId: "1:827669200565:web:b2f6c69dfab25ac4"
    };
    let password = conditions.parameters.password
    console.log(password)
    // password = password + conditions.parameters.password
    // password = password.toString().hashCode() % 100009
    // response.set('Cache-Control','public, max-age=300, s-maxage=600');
    if (conditions.type !== 'POST') {
        // console.log(conditions);
        return cors(request, response, () => {
            response.status(200).send({message : 'HOW ON EARTH YOU EXISTS...'})
        });
    }
    else if (password !== 50959 && password !== -8354) {
        // console.log(conditions);
        return cors(request, response, () => {
            response.status(200).send({message : 'LANGUAGE WILL BE BADASS NOW :)'})
        });
    }
    const client = firebaseConfigJSON;
    return cors(request, response, () => {
        response.status(200).send(client)
    });
});

exports.helloworld = functions.https.onRequest((resquest,response) => {
    var http = require('http');
    var str = '';
    
    var options = {
            host: 'developerswork.online',
            path: ''
    };
    callback = function(res) {
    
        res.on('data', function (chunk) {
            str += chunk;
        });
        
        res.on('end', function () {
            // console.log(req.data);
            console.log(str);
            // your code here if you want to use the results !
            response.send(str);
        });
    }
      
    var req = http.request(options, callback).end();
})
