import React from 'react';

import Main from './Main';
import Footer from './Components/Footer'

class App extends React.Component {
  state = {}
  render(){
    return (
      <div className="App">
        <Main/>
      
        <Footer/>
      </div>
    );
    }
}

export default App;
