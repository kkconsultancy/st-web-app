import React from 'react';
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom';


import Firebase from './Middleware';
import Navbar from './Components/Navbar';



import Login from './Components/Signin';
import Home from './Components/Home';
import Appointment from './Components/Appointment'

// import ChangePassword from './Components/ChangePassword';
// import Users from './Components/Users';
// import Catalogue from './Components/Catalogue'
// import Billing from './Components/Billing'
// import Reports from './Components/Reports'


class App extends React.Component {
  state = {}

  componentWillMount(){
    this.firebase = new Firebase()
  }

  componentDidMount = () => {
    this.firebase.getFirebaseConfig((response) => {
      this.setState({
        response : response
      })
    })
  }
  // componentDidUpdate = () => {
  //   console.log(this.state)
  // }

  render(){
    if(!this.firebase.state.auth)
      return(
        <h1 align="center">
          loading...
        </h1>
      )
    else if(this.firebase.state.auth.currentUser)
      return (
        <BrowserRouter className="App">
          
          <Navbar middleware={this.firebase}/>
        </BrowserRouter>
      )
    else
        return(
          <div>
            <BrowserRouter className="App">
              {/* <Navbar/> */}
              <Switch>
              <Route exact path="/Appointment" component={() => <Appointment middleware={this.firebase}/>} />
                <Route exact path="/" component={() => <Home middleware={this.firebase}/>} />
                <Route path="/login" component={() => <Login middleware={this.firebase}/>} />
                <Redirect exact from="/logout" to="/"/>
              </Switch>
            </BrowserRouter>
          </div>
        )
  }
}

export default App;
