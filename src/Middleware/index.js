import Axios from 'axios'
import client from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/storage';

import Controller from './controller'
import Reports from './reports'

class Middleware{
    state = {
        firebase : client,
        ready : false
    }
    constructor(){
        this.middleware = new Controller(this.state)
        this.reports = new Reports(this.state)
        
        this.serverURL = this.middleware.serverURL
        this.signin = this.middleware.signin
        this.getFirebaseConfig = this.middleware.getFirebaseConfig
        this.intialiseApp = this.middleware.intialiseApp
        this.signout = this.middleware.signout
        this.changePassword = this.middleware.changePassword
        this.getReports = this.reports.getReports
        this.userTransactions = this.reports.userTransactions


        // this.createAppointment({
        //     phoneNumber : "+919949582550",
        //     displayName : "gopinadh",
        //     date : "3-1-2020",
        //     time : "8:00PM",
        //     timestamp : new Date().toUTCString(),
        //     gender : "MALE",
        //     branch : "KAKINADA-2"
        // },(res) => {
        //     console.log(res)
        // })
    }

    resetPassword = (data,callback) => {
        // console.log(data)
        this.state.auth.currentUser.getIdToken().then(token => {
            data.uid = this.state.auth.currentUser.uid
            Axios.post(this.serverURL+'/api/v1/'+token+'/resetPassword',data)
            .then(res => {
                // console.log(res)
                callback(res)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }

    createItem = (data,callback) => {
        this.state.auth.currentUser.getIdToken().then(token => {
            data.uid = this.state.auth.currentUser.uid
            Axios.post(this.serverURL+'/api/v1/'+token+'/createItem',data)
            .then(res => {
                // console.log(res)
                callback(res)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }

    removeItem = (data,callback) => {
        this.state.auth.currentUser.getIdToken().then(token => {
            data.uid = this.state.auth.currentUser.uid
            Axios.post(this.serverURL+'/api/v1/'+token+'/removeItem',data)
            .then(res => {
                // console.log(res)
                callback(res)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }

    getServices = (callback) => {
        const data = {
            uid : this.state.user.uid
        }
        Axios.post(this.serverURL+'/api/v1/getServices',data)
        .then(res => {
            // console.log(res)
            callback(res.data)
        }).catch(err => {
            // console.log(err)
            callback({
                code : 'crashed',
                message : 'Problem with something'
            })
        })
    }

    getUsers = (callback) => {
        this.state.auth.currentUser.getIdToken().then(token => {
            let data = {
                uid : this.state.auth.currentUser.uid,
                token : token
            }
            Axios.post(this.serverURL+'/api/v1/getUsers',data)
            .then(res => {
                // console.log(res.data)
                res.data = res.data.filter(element => {
                    if(this.state.user.role === "OPERATOR"){
                        return element.role !== "ADMIN"
                    }else if(this.state.user.role === "ADMIN"){
                        return true
                    }
                    return false
                })
                callback(res)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {
            console.log(err)
        })
    }

    getUid = (uid,callback) => {
        // console.log(data)
        // console.log(callback)
        const data = {
            uid : uid
        }
        Axios.post(this.serverURL+'/api/v1/getUid',data)
        .then(res => {
            // console.log(res)
            if(!res.data.code)
                callback(res)
            else
                callback(res.data)
        }).catch(err => {
            console.log(err)
            callback({
                code : 'crashed',
                message : 'Problem with something'
            })
        })
    }

    getItems = (callback) => {
        // console.log(data)
        // console.log(callback)
        this.state.auth.currentUser.getIdToken().then(token => {
            let data = {
                uid : this.state.auth.currentUser.uid,
                token : token
            }
            Axios.post(this.serverURL+'/api/v1/getItems',data)
            .then(res => {
                // console.log(res)
                if(!res.data.code){
                    
                    callback(res)
                }
                else
                    callback(res.data)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {
            console.log(err)
        })
    }

    addUser = (data,callback) => {
        this.state.auth.currentUser.getIdToken().then(token => {
            data.uid = this.state.auth.currentUser.uid
            Axios.post(this.serverURL+'/api/v1/'+token+'/addUser',data)
            .then(res => {
                // console.log(res)
                if(!res.data.code)
                    callback(res)
                else
                    callback(res.data)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }

    updateUser = (data,callback) => {
        this.state.auth.currentUser.getIdToken().then(token => {
            data.uid = this.state.auth.currentUser.uid
            // console.log(data)
            Axios.post(this.serverURL+'/api/v1/'+token+'/updateUser',data)
            .then(res => {
                // console.log(res)
                if(!res.data.code)
                    callback(res)
                else
                    callback(res.data)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }

    makePayment = (data,callback) => {
        // console.log(data)
        this.state.auth.currentUser.getIdToken().then(token => {
            let date = new Date()
            data.uid = this.state.auth.currentUser.uid
            data.date = {
                year : date.getFullYear(),
                month : date.getMonth()+1,
                day : date.getDate()
            }
            data.timestamp = date.toUTCString()
            // console.log(data)
            Axios.post(this.serverURL+'/api/v1/'+token+'/securePayment',data)
            .then(res => {
                // console.log(res)
                if(!res.data.code)
                    callback(res)
                else
                    callback(res.data)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }

    createAppointment = (data,callback) => {
        Axios.post(this.serverURL+'/api/v1/public/createAppointment',data)
            .then(res => {
                // console.log(res)
                if(!res.data.code)
                    callback(res)
                else
                    callback(res.data)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
    }

}
export default Middleware