import Axios from 'axios'
import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/storage';

if(window.location.hostname !== "sparkletwilight.developerswork.online")
    if(window.location.hostname !== "localhost")
        window.location.hostname = "sparkletwilight.developerswork.online"

// eslint-disable-next-line no-extend-native
String.prototype.hashCode = function (key = 26) {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << key) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
class Controller{

    constructor(state){
        this.serverURL = "http://localhost:5001/sparkle-twilight/us-central1"
        this.serverURL = "https://us-central1-sparkle-twilight.cloudfunctions.net"
        this.state = state
        this.state.serverURL = this.serverURL
    }

    signin = (data,callback) => {
        // console.log(this.state)
        // console.log(data)
        this.state.auth.signInWithEmailAndPassword(data.email, data.password)
        .then(res => {
            // console.log(res.user)
            callback(res)
            //callback(res.user);
            // this.state.firestore.collection('PROFILES').doc(res.user.uid).get()
            // .then(result => {
            //     localStorage.setItem(res.user.uid, JSON.stringify(result.data()))
            // }).catch(err => {
            //     console.log(err)
            // })
        })
        .catch(err => {
            console.log(err)
            callback(err)
            // callback({
            //     error: true,
            //     code: err.code,
            //     message: err.message
            // })
        });
    }
    signout = () => {
        return this.state.auth.signOut();
    }
    changePassword = (data,callback) => {
        // this.signout()
        this.state.auth.signInWithEmailAndPassword(this.state.user.email,data.opassword)
        .then(response => {
            // console.log(response.user)
            return response.user.updatePassword(data.password).then(res => {
                // console.log(res)
                return callback(res)
            }).catch(err => {
                console.log(err)
                return callback(err)
            })
        }).catch(err => {
            console.log(err)
            return callback(err)
        })
    }

    intialiseApp = (JSONConfig,callback) => {
        // console.log(JSONConfig)
        this.state.firebase.initializeApp(JSONConfig)
        this.state.auth = this.state.firebase.auth();
        this.state.firestore = this.state.firebase.firestore();
        this.state.database = this.state.firebase.database();
        this.state.storage = this.state.firebase.storage();
        this.state.ready = true
        callback(this.state.ready)

        let scope = this
        this.state.auth.onAuthStateChanged((user) => {
            // console.log(user)
            if (user) {
                user.getIdToken().then(t => 
                    t//console.log(t)
                ).catch(err => {
                    console.log(err)
                })
                this.state.firestore.collection('USERS').doc(this.state.auth.currentUser.uid).get()
                .then(doc => {
                    scope.state.user = doc.data()

                    callback(user);
                }).catch(err => console.log(err))
            } else {
                callback(user);
            }
        });
    }

    getFirebaseConfig = (callback) => {
        const expire = new Date().getTime() - localStorage.getItem('sparkleTwilight_firbaseConfigT')
        if(parseInt(expire)/(1000*60*60) >= 1)
            localStorage.removeItem('sparkleTwilight_firbaseConfig')
        let JSONConfig = JSON.parse(localStorage.getItem('sparkleTwilight_firbaseConfig'))
        // console.log(window.location.hostname)
        // console.log(JSONConfig)
        if(JSONConfig && window.location.hostname !== "localhost"){
            this.intialiseApp(JSONConfig,callback)
        }
        else{
            let id = document.getElementById('developerswork')
            let code  = 0
            if(id != null){
                // console.log(id.outerHTML)
                id = id.outerHTML.toString().toLowerCase()
                code = id.hashCode(id.length) % 100009
                // console.log(code)
                code = code + window.location.hostname
                code = code.hashCode() % 100009
            }
            // console.log(code)
            // if(window.location.hostname !== "sparkletwilight.developerswork.online")
            //     if(window.location.hostname !== "localhost")
            //         window.location.hostname = "sparkletwilight.developerswork.online"
            // console.log(window.location.hostname)
            Axios.post(this.serverURL+"/getFirebaseConfig",{password : code})
            .then(res => {
                // console.log(res)
                if(res.data){
                    if (!res.data.apiKey){
                        throw new Error('something failed')
                    }
                }
                localStorage.setItem('sparkleTwilight_firbaseConfig',JSON.stringify(res.data))
                localStorage.setItem('sparkleTwilight_firbaseConfigT',new Date().getTime().toString())
                this.intialiseApp(res.data,callback)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'network/connection-failed',
                    message : 'Sorry for the trouble seems to be services are not working'
                })
            });
        }
    }
}

export default Controller;