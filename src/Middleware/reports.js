import Axios from 'axios'

class Reports{
    constructor(state){
        this.state = state
        this.serverURL = this.state.serverURL
    }
    getReports = (callback) => {
        const data = {
            uid: this.state.user.uid,
        }

        this.state.auth.currentUser.getIdToken().then(token => {
            Axios.post(this.serverURL + '/api/v1/' + token + '/getReports', data)
                .then(res => {
                    // console.log(res)
                    callback(res.data)
                }).catch(err => {
                    console.log(err)
                    callback({
                        code: 'crashed',
                        message: 'Problem with something'
                    })
                })
        }).catch(err => { })
    }

    getAppointments = (data,callback) => {
        Axios.post(this.serverURL+'/api/v1/public/getAppointments',data)
        .then(res => {
            // console.log(res)
            if(!res.data.code)
                callback(res.data)
            else
                callback(res.data)
        }).catch(err => {
            console.log(err)
            callback({
                code : 'crashed',
                message : 'Problem with something'
            })
        })
    }

    userTransactions = (callback) => {
        const date = new Date()
        const data = {
            uid: this.state.user.uid,
            date : {
                year: date.getFullYear(),
                month: date.getMonth()+1,
                day: date.getDate()
            },
            operatorId : this.state.user.uid
        }
         
        this.state.auth.currentUser.getIdToken().then(token => {
            Axios.post(this.serverURL + '/api/v1/' + token +'/getReports',data)
            .then(res => {
                // console.log(res)
                callback(res.data)
            }).catch(err => {
                console.log(err)
                callback({
                    code : 'crashed',
                    message : 'Problem with something'
                })
            })
        }).catch(err => {})
    }
}

export default Reports