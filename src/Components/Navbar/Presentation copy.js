import React from 'react';
import {Route} from 'react-router-dom';
import {Switch,Link,Redirect} from 'react-router-dom';

import ChangePassword from '../ChangePassword';
import Users from '../Users';
import Catalogue from '../Catalogue'
import Billing from '../Billing'
import Reports from '../Reports'
import './index2.css';

class Presentation extends React.Component {
    
    render(){
		console.log(this.props)
      return (
        
		

<div className="bg">
	<div className="navbar2">
		<div class="containerinnavbarpage">
			<div class="brand">
				<div className="logobrand">
					<img alt="logo" style={{maxWidth:"50%",maxHeight:"10vh"}} src="https://bit.ly/2ms5Krk"/>
				</div>
			</div>
			<div className="hello1">
				<button  class="btn btn-dark dropdrown-toggle" data-toggle="collapse" data-target="#c0">
						<div className="inside">Hello,{this.props.middleware.state.user.displayName}</div>
					</button>
				<div id="c0" class="collapse">
					<div >
						<button class="btn btn-dark d-block  mt-2" data-target="#gk" data-toggle="collapse" >
							<Link to="/change-password" style={{color:"white"}}>ChangePassword</Link>
						</button>
					</div>
					<div >
						<button class="btn btn-dark d-block  mt-2" data-target="#gk" data-toggle="collapse" >
							<Link to="/logout" onClick={this.props.middleware.signout} style={{color:"white"}}>Logout</Link>
						</button>
					</div>
				</div>
			</div>
			
			<div className="side">
			<div className="row" style={{marginLeft:"90px"}}>
			<div className="col-md-8 col-lg-2">
				<div className="btn-groups ml-5">
					<button onClick={this.props.handleMain1} id="main3" className="btn btn-sm mt-1" data-toggle="collapse" data-target="#bu1" style={{backgroundColor:"black",width:"135px"}}>
						<h1 style={{fontSize:"25px"}}>Billing</h1>
					</button>
					<div id="bu1" className="collapse">
						<div class="body1" id="body1">
							<Link to="/billing/add" style={{color:"Black"}}>
								<button className="btn btn-lg d-block mt-2" data-target="#mah1" data-toggle="collapse" style={{width:"136px",backgroundColor:"#fc0fc0"}}>Cart</button>
							</Link>
							<Link to="/billing/view" style={{color:"Black"}}>
								<button className="btn btn-lg d-block mt-2" data-target="#mah2" data-toggle="collapse" style={{width:"136px",backgroundColor:"#fc0fc0"}}>View</button>
							</Link>
							<Link to="/billing/payment"style={{color:"Black"}}>
								<button className="btn btn-lg d-block mt-2" data-target="#mah3" data-toggle="collapse" style={{width:"136px",backgroundColor:"#fc0fc0"}}>Payment</button>
							</Link>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-lg-2 ">
				<div className="btn-groups ml-5">
					<button onClick={this.props.handleMain2} id="main2" class="btn  btn-sm mt-1 " data-toggle="collapse" data-target="#bu2"  style={{backgroundColor:"black",width:"135px"}}>
						<h1 style={{fontSize:"25px"}}>Catalogue</h1>
					</button>
					<div id="bu2" class="body2 collapse">
						<div class="body2" id="body2">
							<Link to="/catalogue/add"style={{color:"Black"}}>
								<button class="btn btn-lg d-block mt-2" data-target="#mah1" data-toggle="collapse" style={{width:"136px",backgroundColor:"#fc0fc0"}}>Add</button>
							</Link>
							<Link to="/catalogue/view" style={{color:"Black"}}>
								<button class="btn btn-lg d-block  mt-2" data-target="#mah2" data-toggle="collapse" style={{width:"136px",backgroundColor:"#fc0fc0"}}>View</button>
							</Link>
							<Link to="/catalogue/remove" style={{color:"Black"}}>
								<button class="btn btn-lg d-block  mt-2" data-target="#mah3" data-toggle="collapse" style={{width:"136px",backgroundColor:"#fc0fc0"}}>Remove</button>
							</Link>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-lg-2">
				<div className="btn-groups ml-5" >
					<button onClick={this.props.handleMain3} style={{backgroundcolor:"red"}} id="main3" class="btn  btn-sm  mt-1" data-toggle="collapse" data-target="#bu3"  style={{backgroundColor:"black",width:"135px"}} >
						<h1 style={{fontSize:"25px"}}>Users</h1>
					</button>
					<div id="bu3" class="collapse ">
						<div class="body3" id="body3">
							<Link to="/users/add"style={{color:"Black"}}>
								<button class="btn btn-lg  d-block  mt-2" data-target="#mah1" data-toggle="collapse" style={{width:"134px",backgroundColor:"#fc0fc0"}}>Add</button>
							</Link>
							<Link to="/users/update"style={{color:"Black"}}>
								<button class="btn btn-lg   d-block  mt-2" data-target="#mah2" data-toggle="collapse" style={{width:"134px",backgroundColor:"#fc0fc0"}}>Update</button>
							</Link>
							<Link to="/users/view"style={{color:"Black"}}>
								<button class="btn btn-lg  d-block  mt-2" data-target="#mah3" data-toggle="collapse" style={{width:"134px",backgroundColor:"#fc0fc0"}}>View</button>
							</Link>
							<Link to="/users/reset-password" style={{color:"Black"}}>
								<button class="btn btn-lg  d-block  mt-2 text-auto custom" data-target="#mah4" data-toggle="collapse" style={{width:"134px",backgroundColor:"#fc0fc0",fontSize:"16px"}}>Reset Password</button>
							</Link>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-lg-2 ">
				<div className="btn-groups ">
					<button onClick={this.props.handleMain3}id="main3" class="btn btn-dark btn-sm mt-1" data-toggle="collapse" data-target="#bu4"  style={{backgroundColor:"black",width:"135px"}}>
						<h1 style={{fontSize:"25px"}}>Report</h1>
					</button>
					<div id="bu4" class="collapse">
						<div class="body4" id="body4">
							<Link to="/Reports/view"style={{color:"Black"}}>
								<button class="btn  btn-lg  b-block mt-2" data-target="#mah1" data-toggle="collapse" style={{width:"135px",backgroundColor:"#fc0fc0"}}>View</button>
							</Link>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
			
					  
			{/* 
			<div class="hello">
				<div class=" dropdown">
					<a class="btn-danger dropdown-toggle btn-dark " data-toggle="dropdown" href="0">HELLO
							</a>
					<ul class="dropdown-menu  btn-primary">
						<button>
							<Link to="/change-password">ChangePassword</Link>
							<span class="glyphicon glyphicon-user"></span>
						</button>
						<br></br>
						<button className="btn btn-warning">
							<a href="3">
								<span class="glyphicon glyphicon-log-in"></span> Change Password
									
									
							
							</a>
						</button>
					</ul>
				</div>
			</div> */}
		
		</div>
	</div>
	<div className="container">
			<Switch>
				<Route path="/users" component={() =>
					<Users middleware={this.props.middleware}/>} 
				/>
				<Route path="/catalogue" component={() =>
					<Catalogue middleware={this.props.middleware}/>} 
				/>
				<Route path="/billing" component={() =>
					<Billing middleware={this.props.middleware}/>} 
				/>
				<Route path="/change-password" component={() =>
					<ChangePassword middleware={this.props.middleware}/>} 
				/>
				<Route path="/reports" component={() =>
					<Reports middleware={this.props.middleware}/>}
				/>
				<Redirect exact from="/logout" to="/login"/>
			</Switch>
		</div>
	</div>

        )
    }
}

export default Presentation;