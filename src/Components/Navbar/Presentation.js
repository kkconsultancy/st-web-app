import React from 'react';
import { Link } from 'react-router-dom';
// import { Switch, Route, Redirect } from 'react-router-dom';


import './index2.css';

class Presentation extends React.Component {

	render() {
		// console.log(this.props)
		return (

			<div className="bg">
					<div class="nnavbarpage">
						<div class="brand">
							<div className="logobrand">
								<img alt="logo" style={{ maxWidth: "50%", maxHeight: "15vh" }} src="https://bit.ly/2ms5Krk" />
							</div>
						</div>
						
						<div className="status">
							<div className="row" style={{ marginLeft: "90px" }}>
								{this.props.nav}
								<div className="statusbutton1">
							<button class="btn btn-dark dropdrown-toggle mt-1" data-toggle="collapse" data-target="#c0">
								<div className="statusbutton">Hello,{this.props.middleware.state.user.displayName}</div>
							</button>
							<div id="c0" class="collapse">
								<div >
									<button class="btn btn-dark d-block mt-2" data-target="#gk" data-toggle="collapse" >
										<Link to="/change-password" style={{ color: "white" }}>ChangePassword</Link>
									</button>
								</div>
								<div >
									<button class="btn btn-dark d-block mt-2" data-target="#gk" data-toggle="collapse" >
										<Link to="/logout" onClick={this.props.middleware.signout} style={{ color: "white" }}>Logout</Link>
									</button>
								</div>
							</div>
						</div>
						
						</div>



						
					</div>
				</div>
				<div className="container">
					{this.props.routes}
				</div>
				
			</div>

		)
	}
}

export default Presentation;