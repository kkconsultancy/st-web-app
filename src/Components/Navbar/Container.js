import React, {Component } from 'react';
import Presentation from './Presentation';
// import {NavLink} from 'react-router-dom';
// import $ from 'jquery'

import { Route } from 'react-router-dom';
import { Switch, Link, Redirect } from 'react-router-dom';
import ChangePassword from '../ChangePassword';
import Users from '../Users';
import Catalogue from '../Catalogue'
import Billing from '../Billing'
import Reports from '../Reports'
class Container extends Component{

    state = {
        services : [],
        show : ""
    }

    show = ""
    handleNavClick = (e) => {
        // console.log(e.target.name)
        this.show = e.target.name === this.show ? false : e.target.name
        this.createNav(this.props.middleware.state.user.services)
    }
   
    componentDidMount = () => {
        if(this.props.middleware){
        if(this.props.middleware.state)
            if(this.props.middleware.state.user){
                // console.log(this.props.middleware)
                this.props.middleware.getServices((action) => {
                    // console.log(action)
                    if(!action.code){
                        this.props.middleware.state.user.services = action
                        // console.log(this.props.middleware.state.user.services)
                        this.setState({
                            status : (
                                <li className="nav-item" onClick={this.props.middleware.signout}>
                                    <Link to="/logout" className="nav-link btn btn-danger">
                                        LOGOUT, {this.props.middleware.state.user.displayName} <span className="glyphicon glyphicon-log-out"/>
                                    </Link>
                                </li>
                            ),
                            routes : (
                                <Switch>
						<Route path="/users" component={() =>
							<Users middleware={this.props.middleware} />}
						/>
						<Route path="/catalogue" component={() =>
							<Catalogue middleware={this.props.middleware} />}
						/>
						<Route path="/billing" component={() =>
							<Billing middleware={this.props.middleware} />}
						/>
						<Route path="/change-password" component={() =>
							<ChangePassword middleware={this.props.middleware} />}
						/>
						<Route path="/reports" component={() =>
							<Reports middleware={this.props.middleware} />}
						/>
						<Redirect exact from="/**" to="/" />
					</Switch>
                            )
                        })
                        // action.data['CHANGE PASSWORD'] = []
                        this.createNav(this.props.middleware.state.user.services)
                    }
                })
            }
        }else{
            this.setState({
                status : (
                    <li className="nav-item">
                        <Link to="/login" className="nav-link btn btn-info">
                            <span className="glyphicon glyphicon-log-in"/> LOGIN
                        </Link>
                    </li>
                )
            })
            const services = {}
            this.createNav(services)
        }
        
    }
    heads = (services,service) => {
        return services[service].map(l => {
            return(
                // <li key={l}>
                    // <NavLink key={l} className="dropdown-item" to={"/"+service.toLowerCase().split(' ').join("-")+"/"+l.toLowerCase().split(' ').join("-")}>
                    //     {l}
                    // </NavLink>
                // </li>
                <Link to={"/" + service.toLowerCase().split(' ').join("-") + "/" + l.toLowerCase().split(' ').join("-")}  key={l}>
                    <button class="buttonheads mt-2">{l}</button>
                </Link>
            )
        })
    }
    createNav = (services = null) => {

        const servArrays = []
        for(let i in services){
            servArrays.push(i)
        }
        
        const li = servArrays.map(l => {
            if(services[l].length > 0)
                return(
                    // <li className="nav-item dropdown" key={l}>
                    //     <Link 
                    //         className="dropdown-toggle nav-link" data-toggle="dropdown" 
                    //     to={"/"+l.toLowerCase().split(' ').join("-")}>{l}</Link>
                    //     <div className="dropdown-menu">{this.heads(services,l)}</div>
                    // </li>
                    <div class="col-md-8 col-lg-2 " key={l}>
                        <div className="btn-groups">
                            <button onClick={this.handleNavClick} style={{ fontSize: "124%" }} name={l} class="buttonmain mt-1">
                                {l}
                            </button>
                            <div class={"collapse "+ (this.show === l ? "show" : "")}>
                                <div>
                                    {this.heads(services, l)}
                                </div>
                            </div>
                        </div>
                    </div>
                )
            return(
                // <li className="nav-item" key={l}>
                //     <NavLink className="nav-link" to={"/"+l.toLowerCase().split(' ').join("-")}>{l}</NavLink>
                // </li>
                <div class="col-md-8 col-lg-2" key={l}>
                    <div className="btn-groups ">
                        <Link to={"/" + l.toLowerCase().split(' ').join("-")} style={{ color: "Black" }}>
                            <button onClick={this.props.handleMain3} id="main3" class="btn btn-dark btn-sm mt-1" data-toggle="collapse" data-target="#bu4" style={{ backgroundColor: "black" }}>
                                <h1 style={{ fontSize: "20px" }}>{l}</h1>
                            </button>
                        </Link>
                    </div>
                </div>
            )
        })
        this.setState({
            nav : li
        })
    }

    render() {
        // console.log(this.props)
        return(
            <Presentation
                middleware={this.props.middleware}
                {...this.state} 
            />
        );
    }
}
export default Container;