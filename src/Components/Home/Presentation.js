import React from 'react';
import { Slide} from 'react-slideshow-image'
import './index4.css';
class Presentation extends React.Component {
 
  render() {
    return (
      
      <div>
        
      <div className="backg">
          <img src='/images/logo.png' alt="logo" class="banner-logo " />
          <a href="/Appointment"><button class="btn-lg text-white ml-5 buttons">Appointment</button></a>
          <a href="/login"><button class="btn-lg text-white ml-5 button3">Login</button></a>
          
        </div>
        {/* ====================services================ */}
        <div className="banner4 mt-2">
                {/* <img id="banner-bg" src='/images/logo.png' alt="logo" class="banner-logo " /> */}
        
        <section id="services" class="services service-section">
  <div class="container">
  <div class="section-header">
              <h2 class="wow fadeInDown animated" >Our Services<hr class="new4"></hr>
                </h2>
               
                
            </div>
    <div class="row">
                <div class="col-md-4 col-sm-6 services text-center"> 
                <div class="servesicons"><span class="fa fa-magic"></span></div>
                  <div class="services-content">
          <h5>Face Masks</h5>
          <p class="text-white">Facemasks are one of the most under rated beauty services.Each mask has hand-picked ingredients chosen for suit specific skin types and address specific skin concerns.</p>
        </div>
      </div>
                <div class="col-md-4 col-sm-6 services text-center">
                <div class="servesicons"><span class="fa fa-cut"></span></div>
                  <div class="services-content">
                  <h5>Hair Style</h5>
          <p class="text-white">Our men’s Grooming service offering has everything you need for creating a whole new look, be it giving you a new hair style, a good facial, changing your beard style or adding a dash of color to complete the makeover.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 services text-center"> <span class="icon icon-tools"></span>
      <div class="servesicons"><span class="fa fa-tint"></span></div>
                  <div class="services-content">
                  <h5>Nail Bar</h5>
          <p class="text-white">Nail art has been a trending beauty and fashion. Getting your nails done for the occasion is nothing unheard of these days. A good old French manicure is almost everyone’s favorite sometimes a dramatic red with a razzle dazzle of golden shimmer on the single nail has been a popular trend. </p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 services text-center"> <span class="icon icon-genius"></span>
      <div class="servesicons"><span class="fa fa-asterisk"></span></div>
                  <div class="services-content">
                  <h5>Grooms Make Up</h5>
          <p class="text-white">The big day is not just the bride's day of glamour. The groom needs to look his best as well. From facials, hair grooming to clean hands and feet, we ensure it all for the D-day.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 services text-center"> <span class="icon icon-megaphone"></span>
      <div class="servesicons"><span class="fa fa-female"></span></div>
                  <div class="services-content">
                  <h5>Body Treatment</h5>
          <p class="text-white">Body services are increasingly gaining popularity and are no longer perceived as just pre-bridal regimes. They are picking up with consumers as a popular choice because they exfoliate and hydrate the skin, leaving it smooth & soft. It is a treatment of the entire body which is like a facial and unlike a massage. </p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 services text-center"> <span class="icon icon-trophy"></span>
      <div class="servesicons"><span class="fa fa-heart"></span></div>
                  <div class="services-content">
                  <h5>Bridal Make Up</h5>
          <p class="text-white">Stunning in its colourful splendor, this look uses bright makeup with a heavy base and ensures a chiseled look. The eyes are done up strikingly with a dark coloured eyeliner, false lashes, kohl and lots of shimmer on the eyelids. The lips are enhanced with a deep shade of colour while the hair is swept up into an updo with matching hair accessories.</p>
        </div>
      </div>
    </div>
  </div>
          </section>
          </div>
        
        {/* ==============BRANCHES &ABOUT================ */}

        <div class="section4 mt-2 ">
            <div class="ji">
                <div className="col-md-12 mt-2">
                <div className="col-lg-12 mt-2">
            <div className="SSlide">
                    <h2>Where you can find Us!</h2><hr class="new4"></hr>
                    <p class="pg text-white">Check the locations from below, near to you</p>
                <Slide>
                    <div className="each-slide">
                        <div>
                            <img src='/images/slidebg23.jpeg' alt="img1" />
                            <h4><a class="pg text-dark" href="https://www.google.com/maps/place/Sparkle+Twilight/@17.0061672,81.7848035,17z/data=!3m1!4b1!4m5!3m4!1s0x3a37a3f4af23a52f:0xe1973246ebad356c!8m2!3d17.0061672!4d81.7869922">Rajahmundry</a></h4>
                            <p class="pg text-white">Kumar Plaza, Above Levis Showroom,Danavaipeta(58.63 km)Rajahmundry 533103.</p>
                        </div>
                        </div>
                    <div className="each-slide">
                        <div>
                            <img src='/images/slidebg24.jpeg' alt="img2" />
                            <h4><a class="pg text-dark" href="https://www.google.com/maps/place/Sparkle+Twilight/@16.9832216,82.2406141,15z/data=!4m2!3m1!1s0x0:0xd36ad63afc3bc55c?sa=X&ved=2ahUKEwj_7qbnjfflAhXH7XMBHXLxD-MQ_BIwHHoECBgQCA">Kakinada-1</a></h4>
                            <p class="pg text-white"> Shop No 2, Jntu Road, Nagamallithota Junction,Kakinada-533003.</p>
                        </div>
                    </div>
                    <div className="each-slide">
                        <div>
                            <img src='/images/slidebg26.jpeg' alt="img3" />
                            <h4 ><a class="pg text-dark" href="https://www.google.com/maps/place/Sparkle+Twilight/@16.9832216,82.2384254,17z/data=!3m1!4b1!4m5!3m4!1s0x3a382846ca1341c7:0xd36ad63afc3bc55c!8m2!3d16.9832216!4d82.2406141">Kakinada-2</a></h4>
                            <p class="pg text-white">1st Floor, Subadhra Arcade, Pithapuram Road, Bhanugudi Junction, Kakinada.</p>
                        </div>
                    </div>
                </Slide>
                       </div> 
            </div>
            </div>
            {/* =========================cliennt testimonals======================= */}
            
                <div className="col-md-12">
              <h1>About Us</h1>
              <hr class="new4"></hr>
                    <div className="hi"> 
                    <div className="col-md-12">
                          <div class="paragraph text-white">                    
          <strong>SPARKLE TWILIGHT</strong> has started its operations from 6/6/2011 at Kakinada with only 6 members of a team. Apart from regular saloon services twilight offer s special treatments like laser, slimming, skin and hair with latest technology. We offer a clean facility that is professionally staffed and consistently provides an environment that caters to you…. our honored guests. <strong>SPARKLE TWILIGHT</strong> provides a comforting, yet refreshing atmosphere which will ease your mind, body and spirit. As part of the competitive and increasingly evolving hair and beauty industry.<br></br> SPARKLE is the newest technology with a continuous up-to-date. We would welcome the opportunity to earn your trust and deliver the best service in the industry.We provide special Skin Treatments, Nail Treatments, Hair Treatments, Saloon Treatments, Obesity, Laser, Body Spa, Nail and Skin & Hair Trichology Clinic.
          <strong>SPARKLE TWILIGHT</strong> offers special treatments like laser, slimming, skin and hair with latest technology.  As a part of the competitive and constantly evolving hair and beauty industry <strong>SPARKLE TWILIGHT</strong> continuous to stay up to date with the newest technology.
                       </div>
                        </div>
                        </div>
                </div>
                </div>
            </div>
   
        {/* ===================ABOUT================================= */}
         {/* =====================services extra================================ */}
         {/* <div id="venue">
        <section class="services-section1 py-5 py-md-0 bg-light">
      <div class="container1">
        <div class="row no-gutters d-flex">
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services1 d-block">
              <div class="icon"><span class="fa fa-magic"></span></div>
              <div class="media-body">
                <h5 class="heading mb-3">Makeup</h5>
                <p>	203 Fake St. Mountain View, San Francisco, California, USA</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services1 d-block">
              <div class="icon"><span class="fa fa-asterisk"></span></div>
              <div class="media-body">
                <h5 class="heading mb-3">Hair Styling</h5>
                <p>A small river named Duden flows by their place and supplies.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services1 active d-block">
              <div class="icon"><span class="fa fa-heart"></span></div>
              <div class="media-body">
                <h5 class="heading mb-3">Nails</h5>
                <p>A small river named Duden flows by their place and supplies.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services1 d-block">
              <div class="icon"><span class="fa fa-user"></span></div>
              <div class="media-body">
                <h3 class="heading mb-3">Body Treatment</h3>
                <p>A small river named Duden flows by their place and supplies.</p>
              </div>
            </div>      
          </div>
        </div>
                    </div>
                    
          </section>
          </div> */}
       {/* ===========================end====================  */}
        <div className="banner1">
        <section id="venue" >
          <div class="section-header1">
            <h1>Gallery</h1><hr class="new4"></hr>
          </div>
          <div class="container-fluid venue-gallery-container">
            <div class="row no-gutters px-2">
              <div class="col-lg-3 col-md-4">
                <div class="venue-gallery">
                  <img src='/images/14.jpg' alt="block-images" className="block-image" />
                </div>
              </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/man2.jpeg'  alt="block-images" className="block-image" />
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/13.jpg'  alt="block-images" className="block-image"/>
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/q.jpeg'  alt="block-images" className="block-image"/>
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/man1.jpg'  alt="block-images" className="block-image" />
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/6.jpg'  alt="block-images" className="block-image"/>
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/7.jpg' alt="block-images" className="block-image" />
              </div>
            </div>
            <div class="col-lg-3 col-md-4">
              <div class="venue-gallery">
                <img src='/images/15.jpg'  alt="block-images" className="block-image" />
              </div>
            </div>
          </div>
          </div>
        </section>
        </div>
         
        {/*==============Last foooter====================*/}
        <div className="banne">
           <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 footer-info">
            </div>

              
          <div class="col-lg-6 col-md-8 footer-contact">
            <h4>Contact Us</h4>
            <p>
            Sparkle Twilight, Shop No 2, Jntu Road, Nagamallithota Junction, Kakinada - 533003.<br></br>
              <strong>Phone:</strong> 081215 38888<br></br>
              <strong>Email:</strong> info@sparkletwilight.com
            </p>
            <div class="social-links">
              <a href="1" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/SparkleTwilightkkd/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="1" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="info@sparkletwilight.com" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="1" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>
                    
          </div>
          
        </div>
      </div>
    </div>

    
      {/* <h2 className="footer-copyright container text-center text-white py-0">
            <a className="text-white" href="https://developerswork.online" target="_blank" rel="noopener noreferrer">
                <img  alt="logo" style={{height:"30px"} }src="https://i0.wp.com/developerswork.online/wp-content/uploads/2019/03/DESIGNS-Copy.png?fit=360%2C60&amp;ssl=1"/>
            </a>
        </h2> */}
    
          </footer>
          {/* ============================================================== */}
        </div>
        
      </div> 
    );
  }
  
}

export default Presentation;