import React from 'react';
import './style.css';
class Presentation extends React.Component {
	state = {
		file : null
	}
	Handlerfile(e){
		let file = e.target.files[0]
		this.setState({file:file})
	}
	
	Handlerupload(e) {
		console.log(this.state);
		
	}
		
  render(){
    return (
		<form className="mt-3 container text-center">
		<iframe id="ifmcontentstoprint" title="ifmcontentstoprint" style={{height: "0px",width: "0px",position: "absolute",display:"none"}}></iframe>
		<div className="card1 ">
			<div className="card-body mb-5 border"style={{padding:"0.5px"}}>
				<div className="card-header text-danger">
					{/* {this.props.error} */}							
				</div>				
				<div className="card-body col-12 clo-md-6 form">
					<div className="col-sm-10 " width="100%">
						<input type="file" multiple name="file" onChange={(e)=>this.Handlerfile(e)} />
						<button type="button" onClick={(e)=>this.Handlerupload(e)}>Upload</button>	
					</div>		
				</div>
			</div>	
		<div className="car border">
			<div className="card-body">
				<div className="row mt-1 mb-3 form-group">
					<div className="col-0"/>
					<div className="col-12">
						<table id="cart" width="100%" className="border bg-white">
							<tbody>
								<tr>
									<th>Filethumbnail</th>
									<th>Filename</th>
									<th>Upload Date</th>
									<th>Remove</th>
									<th></th>
								</tr>																
							</tbody>
						</table>
					</div>
				</div>	
			</div>
		</div>
		</div>
		</form>
	
			)
		}
	}
	
export default Presentation; 