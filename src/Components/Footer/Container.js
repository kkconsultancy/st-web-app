import React,{Component} from 'react'
import Presentation from './Presentation';
import GoogleAd from '../GoogleAd'

class Container extends Component{
    componentDidMount(){
        setInterval(()=>{
            this.setState({
                ad : <GoogleAd/>
            })
        },30000)
    }

    render(){
        return(
            <Presentation
                {...this.state}
            />
        )        
    }
}

export default Container