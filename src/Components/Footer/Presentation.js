import React from 'react'
import './style.css'

class Presentation extends React.Component{
  render(){
    return(
      <footer className="page-footer justify-content-center sticky-bottom pagination-centered mx-5">
          {/* <div className="container">
              <div className="row text-center">
                  <div className="footer-copyright container text-white">
                      <img alt="logo" src="https://firebasestorage.googleapis.com/v0/b/sparkle-twilight.appspot.com/o/IMAGES%2FLogoMakr_7b6EzY.png?alt=media&token=ab962ee2-548c-4c2f-8512-ddfd2d43663f"/>
                      <p>Give a spark to your beauty</p>
                  </div>
              </div>
          </div> */}
          <h1 className="footer-copyright container text-center text-white py-3" id="developerswork">
              <img src={'https://bitbucket.org/kkconsultancy/web-frontend/raw/e9d048002840266b1c6dd3744c19a1e1105117a7/src/Assets/Images/brandLogo.png'} className="brandLogo" alt="Developers@Work" />
              <a href="https://developerswork.online" target="_blank" rel="noopener noreferrer">
                  <img src="https://i0.wp.com/developerswork.online/wp-content/uploads/2019/03/DESIGNS-Copy.png?fit=360%2C60&amp;ssl=1" alt="DevelopersWork"/>
              </a>
          </h1>
          {this.props.ad}
      </footer>
    )
  }
}


export default Presentation