import React from 'react';
import {Route,Redirect,Switch} from 'react-router-dom'
// import GoogleAd from '../GoogleAd'

import ADD from './ADD'
import VIEW from './VIEW'
import REMOVE from './REMOVE'

class Presentation extends React.Component {

    render(){
        let action = this.props.middleware.state.user.services
        for(let i in action)
            if(i === "CATALOGUE")
        return (
              <div>
                  <Switch>
                    <Route path="/catalogue/add" component={() => <ADD middleware={this.props.middleware}/>}/>
                    <Route path="/catalogue/view" component={() => <VIEW middleware={this.props.middleware}/>}/>
                    <Route Path="/catalogue/remove" component={() => <REMOVE middleware={this.props.middleware}/>}/>
                    <Redirect from="**" to="/catalogue/add"/>
                </Switch>
              </div>
        )
        return (
            <h1>404</h1>
        )
    }
}

export default Presentation;