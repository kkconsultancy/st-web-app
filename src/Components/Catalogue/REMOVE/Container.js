import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        availableItems : []
    }
    componentDidMount = () => {
        this.getItems()
    }
    submit = (e) => {
        e.preventDefault();
        console.log(this.state)
        const data = {
            id : this.state.productId
        }
        this.props.middleware.removeItem(data,(response) => {
            console.log(response)
        })
    }
    getItems = () => {
        let scope = this
        this.props.middleware.getItems((response) => {
            if(!response.code && response.data){
                response.data.push({
                    name : ""
                })
                scope.setState({
                    availableItems : response.data
                })
            }
            this.setState({
                items : this.loadItems(0,'')
            })
        })
    }

    categorieItems = {}
    loadItems = (nextLoad,parent = "") => {
        // let array = []
        if(parent === ""){
            this.categorieItems = [
            <option
                value=""
                key = "NONE"
                onClick={this.loadCategories}>
                    None
            </option>
            ]
        }
        let str = ""
        for(let i=0;i<nextLoad;i++)
            str += " - "
        this.state.availableItems.filter(item => item.parent === parent).map(item => {
            // console.log(item)
                console.log(item)
                this.categorieItems.push(
                    <option 
                        key={item.parent+"@"+item.id} 
                        value={nextLoad+"@"+item.id+"@"+item.parent} 
                        onClick={this.loadCategories}>
                            {str + item.name}
                    </option>
                )
                this.loadItems(nextLoad+1,item.id)
                // if(arr.length > 0)
                //     this.categorieItems.push(arr)
                return null
        })
        return this.categorieItems
    }
    loadCategories = (e) => {
        console.log(e.target.value)
        // const nextLoad = e.target.value.split('@')[0]
        const id = e.target.value.split('@')[1]
        // const parent = e.target.value.split('@')[2]
        this.setState({
            [e.target.name] : id
        })
        console.log(id)
    }

    render() {  
        return(
            <Presentation 
            {...this.state}
            submit = {this.submit}
            loadCategories = {this.loadCategories}
            />
        );
    }
}
export default Container;