import React from 'react';

class Presentation extends React.Component {

    render(){
        return (
            
<form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
	<div className="card0 mt-5 mb-5">
		<div className="card-body">
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<h4 className="card-title text-dark">REMOVE CATEGORY / ITEM</h4>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-2" />
				<div className="col-4 col-lg-2">
					<label>Categories / Items</label>
				</div>
				<div className="col-8 col-lg-5">
					<select defaultValue="" className="form-control" name="productId" onChange={this.props.loadCategories}>
                                    {this.props.items}
                                </select>
				</div>
				<div className="col-0 col-lg-3" /></div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-lg-4 col-2" />
				<div className="col-lg-2 col-4">
					<button className="btn btn-outline-primary btn-lg active mt-5" type="submit" onClick={this.props.submit}>REMOVE</button>
				</div>
				<div className="col-lg-2 col-4">
					<button className="btn btn-outline-dark btn-lg active mt-5" type="reset" variant="primary" disabled={this.props.btnDisabled} >RESET</button>
				</div>
				<div className="col-lg-4 col-2" /></div>
		</div>
	</div>
</form>
        )
    }
}

export default Presentation;