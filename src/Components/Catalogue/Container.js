import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        availableItems : []
    }
    render() {  
        return(
            <Presentation 
            {...this.state}
            middleware = {this.props.middleware}
            />
        );
    }
}
export default Container;