import React from 'react';

class Presentation extends React.Component {

    render(){
        return (
            
<div className="bg1">
	<form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
		<div className="card0 mt-5 mb-5">
			<div class="card-body">
				<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
					<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
				</div>
				<h4 className="card-title text-dark">CREATE CATEGORY / ITEM</h4>
				<div className="row mt-2 mb-2 form-group">
					<div className="col-0 col-lg-2"/>
					<div className="col-3 col-lg-2">
						<label>Name of the Product</label>
					</div>
					<div className="col-4 col-lg-4">
						<input className="form-control" type="text" name="productName" onChange={this.props.update} placeholder="HAIR DYE"/>
					</div>
					<div className="col-0 col-lg-3"/></div>
				<div className="row mt-3 mb-3 form-group">
					<div className="col-0 col-lg-2"/>
					<div className="col-4 col-lg-2">
						<label>Category of Product</label>
					</div>
					<div className="col-4 col-lg-4">
						<select defaultValue="" className="form-control" name="parent" onChange={this.props.loadCategories}>
                                    {this.props.items}
                                </select>
					</div>
					<div className="col-0 col-lg-3"/></div>
				<div className="row mt-3 mb-3 form-group">
					<div className="col-0 col-lg-2"/>
					<div className="col-4 col-lg-2">
						<label>Price</label>
					</div>
					<div className="col-4 col-lg-4">
						<input type="text" className="form-control" name="price" onChange={this.props.update} placeholder="999"/>
					</div>
					<div className="col-0 col-lg-3"/></div>
				<div className="row mt-3 mb-3 form-group">
					<div className="col-0 col-lg-2"/>
					<div className="col-4 col-lg-2">
						<label>Description of Product</label>
					</div>
					<div className="col-4 col-lg-4">
						<textarea type="text" rows="5" className="form-control" name="description" onChange={this.props.update} placeholder="BRIEF INFORMATION ABOUT THE PRODUCT"/>
					</div>
					<div className="col-0 col-lg-3"/></div>
				<div className="row mt-3 mb-3 form-group">
					<div className="col-lg-4 col-2"/>
					<div className="col-lg-2 col-4">
						<button className="btn btn-outline-primary btn-lg active" type="submit" onClick={this.props.submit}>ADD</button>
					</div>
					<div className="col-lg-2 col-4">
						<button className="btn btn-outline-dark btn-lg active" type="reset" variant="primary" disabled={this.props.btnDisabled} >RESET</button>
					</div>
					<div className="col-lg-4 col-2"/></div>
			</div>
		</div>
	</form>
</div>
        )
    }
}

export default Presentation;