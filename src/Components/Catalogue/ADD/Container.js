import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        parent : "",
        productName : "",
        price : "",
        description: "",
        availableItems : []
    }
    componentDidMount = () => {
        this.getItems()
    }
    submit = (e) => {
        e.preventDefault();
        // console.log(this.state)
        for(let i in this.state){
            if(!this.validate({
                target : {
                    name : i,
                    value : this.state[i]
                }
            })) return;
        }
        const data = {
            name : this.state.productName,
            price : this.state.price,
            parent : this.state.parent,
            description : this.state.description
        }
        this.props.middleware.createItem(data,(response) => {
            // console.log(response)
            if(!response.code){
                this.setState({
                    parent : "",
                    productName : "",
                    price : "",
                    description: "",
                    error : "ITEM ADDED SUCESSFULLY"
                })
                // this.shouldComponentUpdate()
                this.getItems()
                this.render()
            }
        })
    }
    validate = (e) => {
        if(e.target.name === 'productName'){
            let reg= /^.{3,}$/
            if(!e.target.value.match(reg)){
                this.setState({
                    error : 'PRODUCT NAME INCORRECT'
                })
                return false
            }
        }
        else if(e.target.name==='price'){
            let reg = /[0-9]+/
            if(!e.target.value.match(reg)){
                this.setState({
                    error : 'PRICE IS INVALID'
                })
                return false
            }else if(parseInt(e.target.value) < 0){
                this.setState({
                    error : 'PRICE IS INVALID'
                })
                return false
            }

        }
        return true
        
    }
    update = (e) => {
        this.setState({
            [e.target.name] : e.target.value ,
            error : ''
        })

        this.validate({
            target : {
                name : e.target.name,
                value : e.target.value
            }
        })
       
    }
    getItems = () => {
        let scope = this
        this.props.middleware.getItems((response) => {
            if(!response.code && response.data){
                response.data.push({
                    name : ""
                })
                scope.setState({
                    availableItems : response.data
                })
            }
            this.setState({
                items : this.loadItems(0,'')
            })
        })
    }

    categorieItems = {}
    loadItems = (nextLoad,parent = "") => {
        // let array = []
        if(parent === ""){
            this.categorieItems = [
            <option
                value=""
                key = "NONE"
                onClick={this.loadCategories}>
                    None
            </option>
            ]
        }
        let str = ""
        for(let i=0;i<nextLoad;i++)
            str += " - "
        this.state.availableItems.filter(item => item.parent === parent).map(item => {
            // console.log(item)
                // console.log(item)
                this.categorieItems.push(
                    <option 
                        key={item.parent+"@"+item.id} 
                        value={nextLoad+"@"+item.id+"@"+item.parent} 
                        onClick={this.loadCategories}>
                            {str + item.name}
                    </option>
                )
                this.loadItems(nextLoad+1,item.id)
                // if(arr.length > 0)
                //     this.categorieItems.push(arr)
                return null
        })
        return this.categorieItems
    }
    loadCategories = (e) => {
        console.log(e.target.value)
        // const nextLoad = e.target.value.split('@')[0]
        const name = e.target.value.split('@')[1]
        // const parent = e.target.value.split('@')[2]
        this.setState({
            [e.target.name] : name
        })
        console.log(name)
    }

    render() {  
        return(
            <Presentation 
            {...this.state}
            submit = {this.submit}
            update = {this.update}
            loadCategories = {this.loadCategories}
            />
        );
    }
}
export default Container;