import React from 'react';

class Presentation extends React.Component {

    render(){
        return (
                
            
<div className="mt-5 mb-5 container text-center">
	<div className="card mt-5 mb-5">
		<div className="card-body">
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<h4 className="card-title text-dark">VIEW CATEGORIES / ITEMS</h4>
                        {/* 
			<div className="row mt-3 mb-3 form-group">
				<div className="col col-lg-8"/>
				<div className="col col-lg-4">
					<div className="input-group">
						<div className="input-group-prepend">
							<span className="input-group-text">@</span>
						</div>
						<input className="btn-outline-secondary btn form-control" onChange={this.props.search} type="text" placeholder="details"/>
					</div>
				</div>
			</div> */}
                        
			<div className="container text-left">
			 {this.props.items}
			
                        </div>
		</div>
	</div>
</div>
        )
    }
}

export default Presentation;