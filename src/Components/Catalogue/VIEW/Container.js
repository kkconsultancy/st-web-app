import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
    }
    componentDidMount = () => {
        this.getItems()
    }
    getItems = () => {
        let scope = this
        this.props.middleware.getItems((response) => {
            if(response.data){
                response.data.push({
                    name : ""
                })
                scope.setState({
                    availableItems : response.data
                })
            }
            this.setState({
                items : this.loadItems(0,'')
            })
        })
    }

    loadItems = (nextLoad,parent = "") => {
        // let array = []
        if(parent === ""){
            this.categorieItems = []
        }
        let str = ""
        for(let i=0;i<nextLoad;i++)
            str += " - "
        this.state.availableItems.filter(item => item.parent === parent).map(item => {
            // console.log(item)
                // console.log(item)
                this.categorieItems.push(
                    <option 
                        key={item.parent+"@"+item.id} 
                        value={nextLoad+"@"+item.id+"@"+item.parent} 
                        onClick={this.loadCategories}
                        >
                            {str + item.name}
                    </option>
                )
                // console.log(this.categorieItems[this.categorieItems.length-1])
                this.loadItems(nextLoad+1,item.id)
                // if(arr.length > 0)
                //     this.categorieItems.push(arr)
                return null
        })
        return this.categorieItems
    }

    search = (e) => {
    }

    render() {  
        return(
            <Presentation 
                {...this.state}
                search = {this.search}
            />
        );
    }
}
export default Container;