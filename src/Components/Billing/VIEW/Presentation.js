import React from 'react';

class Presentation extends React.Component {
    render(){
        return (
            
<form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
	<iframe id="ifmcontentstoprint" title="ifmcontentstoprint" style={{height: "0px",width: "0px",position: "absolute",display:"none"}}></iframe>
	<div className="card mt-5 mb-5">
		<div className="card-body">
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<h4 className="card-title text-dark">PAYMENT</h4>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-1"/>
				<div className="col-10">
					<table width="100%" border="1">
						<tbody>
							<tr>
								<th>PRINT RECEIPT</th>
								<th>TRANSACTION ID</th>
								<th>CUSTOMER MOBILE NO.</th>
								<th>ITEMS PURCHASED</th>
								<th>COST</th>
								<th>DISCOUNT</th>
								<th>PAID</th>
								<th>TIME</th>
							</tr>
                                    {this.props.transactionsToday}
                                
						</tbody>
					</table>
				</div>
				<div className="col-1" /></div>
		</div>
	</div>
</form>
        )
    }
}

export default Presentation;