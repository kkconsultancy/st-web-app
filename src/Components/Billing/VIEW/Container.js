import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {}
    componentDidMount(){
        this.pri = document.getElementById("ifmcontentstoprint").contentWindow;
        this.pri.addEventListener('afterprint',this.setHeader()) 
        this.setHeader()
        this.props.middleware.reports.userTransactions(response => {
            if(response.code)
                return this.setState({
                    error : "UNABLE TO GET THE REQUIRED DATA"
                })
            const data = response.transactions
            if(!data || data.length === 0){
                return this.setState({
                    error : "NO TRANSACTIONS DONE YET TO SHOW TODAY"
                })
            }
            console.log(data)
            this.setState({
                data : data
            })
            const table = []
            for(let i in data){
                console.log(data[i])
                const divs = []
                for(let k in data[i].items){
                    // console.log(k)
                    if(data[i].items[k]){
                        const jsx = (
                            <div key={k}>
                                {data[i].items[k].name}
                            </div>
                        )
                        divs.push(jsx)
                    }
                }
                console.log(divs)
                    const jsx = (
                        <tr id={i} key={i}>
                            <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td>
                            <td>{data[i].transactionNumber}</td>
                            <td>{data[i].customerId}</td>
                            <td>{divs}</td>
                            <td>{data[i].total}</td>
                            <td>{data[i].discount}</td>
                            <td>{data[i].payment}</td>
                            <td>{data[i].timestamp}</td>
                        </tr>
                    )
                    table.push(jsx)
            }
            this.setState({
                transactionsToday : table
            })
        })

    }
    setHeader = () => {
        this.pri.document.open()
        this.pri.document.clear()
        this.pri.document.write("<div style='border:1px solid black;padding:5%;'>")
        this.pri.document.write("<div align='center'><img alt='logo' height='50px' src='https://bit.ly/2ms5Krk'/></div>")
        this.pri.document.write("<div align='center'><stong>BILL</strong></div><hr/>")
    }
    printBill = (e) => {
        const data = this.state.data[e.target.value]

        this.pri.document.write("<div align='center'>Customer: <strong>"+data.customerId+"</strong></div><hr/>")

        this.pri.document.write("<strong><p>Reciept Id: "+data.transactionNumber+"</p>")
        this.pri.document.write("<p align='right'>Date: "+data.date.day+"-"+(parseInt(data.date.month)+1)+"-"+data.date.year+"</p><p>Beautician: "+(data.beauticianId?data.beauticianId:'---')+"</p></strong>")

        var content = '<table width="100%"><tbody><tr><th>NAME</th><th>COST</th></tr>'
        console.log(data.items)
        for(let i in data.items){
            const item = data.items[i]
            if(item)
                content += "<tr><td>"+item.name+"</td><td>"+item.price+"</td></tr>"
        }
        content += "<tr><td><strong>Total</strong></td><td><strong>"+data.total+"</strong></td></tr><hr>"
        content += "<tr><td><strong>Discount</strong></td><td><strong>"+data.discount+"</strong></td></tr>"
        content += "<tr><td><strong>PAID</strong></td><td><strong>"+data.payment+"</strong></td></tr>"
        content += '</tbody></table>'
        this.pri.document.write(content)
        this.pri.document.write("<style>tr{border:2px solid grey}</style><hr/>")
        this.pri.document.write("<h5 align='right'>SIGNATURE<br/>("+data.operatorId+")</h5>")

        this.pri.document.write("</div>")

        this.pri.document.close()
        this.pri.focus();
        this.pri.print();
        this.pri.document.clear()

    }
    render() {  
        return(
            <Presentation 
                {...this.state}
            />
        );
    }
}
export default Container;