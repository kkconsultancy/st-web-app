import React from 'react';

class Presentation extends React.Component {
    render(){
        return (
            
<form onSubmit={this.props.submit} className="mt-3 container text-center">
	<iframe id="ifmcontentstoprint" title="ifmcontentstoprint" style={{height: "0px",width: "0px",position: "absolute",display:"none"}}></iframe>
	<div className="card1">
		<div className="card-body"style={{padding:"0.5px"}}>
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<h4 className="card-title text-dark">PAYMENT</h4>
			<div className="row mx-3 form">
				{/* <div className="col-0 col-lg-1"/> */}
				<div className="col-2">
					<label>Customer Mobile No.</label>
				</div>
				<div className="col-4 input-group">
					<div class="input-group-text">
						<div class="input-group-text">+91</div>
						<input  type="text" disabled={this.props.btnDisabled} className="form-control" name="username" placeholder="9999999990" onChange={this.props.update} />
					</div>
				</div>
				<div className="col border-2">
					{this.props.customerInfo}
				</div>
			</div>
		</div>

	<div className="car">
		<div className="card-body">
			<div className="row mt-1 mb-3 form-group">
				<div className="col-0"/>
				<div className="col-12">
					<table id="cart" width="100%" className="border bg-white">
						<tbody>
							<tr>
								<th>NAME</th>
								<th>PRICE</th>
								<th>BEAUTICIAN</th>
								<th>REFERED  BY</th>
								<th></th>
							</tr>
                            {this.props.cartBlock}
                                    
						</tbody>
					</table>
				</div>
				<div className="col-0" /></div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-1" />
				<div className="col-3 col-lg-2">
					<label>Discount</label>
				</div>
				<div className="col-3 col-lg-2">
					<input type="radio" className="form-control" disabled={this.props.btnDisabled} checked={this.props.discountType === "percentage"} onChange={this.props.update} value="percentage" name="discountType" id="percentageD"/>
					<label htmlFor="percentageD">Percentage</label>
				</div>
				<div className="col-3 col-lg-2">
					<input type="radio" className="form-control" disabled={this.props.btnDisabled} onChange={this.props.update} value="amount" name="discountType" id="amountD"/>
					<label htmlFor="amountD">Amount</label>
				</div>
				<div className="col col-lg-5">
					<input type="text" className="form-control" disabled={this.props.btnDisabled} name="discount" value={this.props.discount} onChange={this.props.update}/>
				</div>
			</div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-1" />
				<div className="col-3 col-lg-2">
					<label>Payment Type</label>
				</div>
				<div className="col-3 col-lg-2">
					<input type="radio" className="form-control" disabled={this.props.btnDisabled} checked={this.props.paymentType === "CARD"} onChange={this.props.update} value="CARD" name="paymentType" id="cardPT" />
					<label htmlFor="cardPT">Debit / Credit Card</label>
				</div>
				<div className="col-3 col-lg-2">
					<input type="radio" className="form-control" disabled={this.props.btnDisabled} onChange={this.props.update} value="CASH" name="paymentType" id="cashPT" />
					<label htmlFor="cashPT">Cash</label>
				</div>
			</div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-1" />
				<div className="col-3 col-lg-2">
					<label>Payment : </label>
				</div>
				<div className="col">
					<strong className="">
                                    {this.props.payable}
                                </strong>
				</div>
			</div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-4" />
				<div className="col-4">
					<button disabled={this.props.btnDisabled} className="btn btn-outline-primary btn-lg active" type="submit" onClick={this.props.submit}>PAY</button>
				</div>
				<div className="col-4" /></div>
		</div>
	</div>
	</div>
</form>

        )
    }
}

export default Presentation;