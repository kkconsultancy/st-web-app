import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        cart : {},
        level : {},
        cartBlock: <tr><td>NO ITEMS IN CART</td></tr>,
        btnDisabled : false,
        discount : 0,
        discountType : "percentage",
        discountAmount : 0,
        payable : 0
    }
    componentDidMount(){
        this.pri = document.getElementById("ifmcontentstoprint").contentWindow;
        this.pri.document.open()
        this.props.middleware.getItems((response) => {
            // console.log(response)
            if(!response.code){
                if(!this.props.middleware.state.database){
                    this.setState({
                        error : "CONNECTION ERROR, PLEASE REFRESH THE PAGE"
                    })
                }
                this.setState({
                    data : response.data,
                    db : this.props.middleware.state.database,
                })
            }
        })
        this.props.middleware.getUsers((response)=>{
            if(!response.code){
                let beauticians = response.data.filter(user => user.role === "BEAUTICIAN").map(user => {
                    return <option key={user.id} value={user.id}>{user.displayName+" ("+user.phoneNumber+")"}</option>
                })
                beauticians = [<option key="" value="">None</option>,...beauticians]
                this.setState({
                    beauticians : beauticians
                })
            }
        })
    }
    validate = (e) => {
        // console.log(e)
        if(e.target.name === "discountType"){
            return this.setState({
                discountAmount : 0,
                discount : 0
            })
        }
        if(e.target.name === "discount"){
            if(!e.target.value.match(/^[0-9]+$/)){
                return this.setState({
                    error : "Discount is in incorrect format"
                })
            }
            else if(this.state.cart.total){
                let amount = parseFloat(this.state.cart.total)/100.0
                e.target.value = e.target.value ? e.target.value : 0
                let discount = this.state.discountType === "percentage" ? e.target.value*amount : e.target.value
                discount = discount > this.state.cart.total ? this.state.cart.total : discount
                // console.log(discount,amount)
                this.setState({
                    discountAmount : discount,
                    payable : Math.round(this.state.cart.total - discount)
                })
            }else{
                return this.setState({
                    discountAmount:0,
                    payable : this.state.cart.total
                })
            }
        }
        if(e.target.name === "beautician" && !e.target.value){
            return this.setState({
                error : "Please select a beautician",
                beautician : "",
                beauticianInfo : ""
            })
        }
        if(e.target.name === "username" || e.target.name === "beautician" ){
            if (e.target.name === "username") e.target.value = "+91" + e.target.value;
            this.setState({
                [e.target.name] : e.target.value
            })
            if(!e.target.value.match(/^\+[0-9]{12}$/) && e.target.name === "username"){
                return this.setState({
                    error : "Mobile number is incorrect",
                    parents : '',
                    customerInfo : "",
                    cart : ""
                })
            }else{
                this.pri.document.open()
                if (this.pri.document.body)
                    this.pri.document.body.innerText = ""
                // this.pri.document.clear();
                this.pri.document.write("<div style='border:1px solid black;padding:5%;'>")
                this.pri.document.write("<div align='center'><img alt='logo' height='50px' src='https://bit.ly/2ms5Krk'/></div>")
                
                this.pri.document.write("<div align='center'><stong>BILL</strong></div><hr/>")
                if(e.target.name === 'beautician'){
                    e.target.value = e.target.value.split(')')[0].split('(')[1]
                }
                this.props.middleware.getUid(e.target.value,(response) => {
                    if(!response.code){
                        const data = response.data
                        if(e.target.name === 'beautician'){
                            return this.setState({
                                beauticianInfo : (
                                    <div>
                                        <div>Name : {data.displayName}</div>
                                        <div>Address : {data.address}</div>
                                        <div>Email : {data.email}</div>
                                    </div>
                                ),
                                beauticianName : data.displayName,
                                error : ''
                            })
                        }
                        // console.log(this.state)
                        this.state.db.ref('/'+data.uid).once('value').then(res => {
                            const val = res.val()
                            if(!val){
                                return this.setState({
                                    error : ""
                                })
                            }
                            let amount = parseFloat(val.total)/100.0
                            let discount = this.state.discountType === "percentage" ? this.state.discount * amount : this.state.discount
                            // console.log(discount,amount)
                            if(val)
                                this.setState({
                                    cart : val,
                                    cartBlock : this.showCart(val),
                                    discountAmount : discount,
                                    payable : Math.round(val.total - discount)
                                })
                            else
                                this.setState({
                                    cart : {items : {},total : ''},
                                    cartBlock : <tr><td>NO ITEMS IN CART</td></tr>,
                                    payable : 0
                                })
                        })
                        this.setState({
                            customerInfo : (
                                <div className="bg-white">
                                    <div><strong>Name : </strong>{data.displayName}</div>
                                    <div><strong>Address : </strong>{data.address}</div>
                                    <div><strong>Email : </strong>{data.email}</div>
                                </div>
                            ),
                            user : response.data,
                            error : ''
                        })
                    }
                })
            }
        }
    }

    update = (e) => {
        this.setState({
            [e.target.name] : e.target.value,
            error : '',
            // payable : 0
        })
        this.validate({target : { name : e.target.name,value : e.target.value}})
    }

    updateCart = (e) => {
        let cart = this.state.cart.items
        // console.log(e.target)
        cart[e.target.id] = false
        let total = 0
        let discountAmount = this.state.discountAmount
        for(let i in cart){
            if(cart[i].price)
                total += parseInt(cart[i].price)
        }
        this.setState({
            totalCost : total
        })
        cart = {items : cart,total : total}
        this.state.db.ref('/'+this.state.user.uid).set(
            {...cart}
        )
        if(this.state.discountType === "percentage")
            discountAmount = discountAmount*(total/100.0)
        return this.setState({
            cart : cart,
            cartBlock : this.showCart(cart),
            payable : Math.round(cart.total - discountAmount),
            discountAmount : discountAmount
        })
    }

    showCart = (cart) => {
        // console.log(cart)
        const cartBlock = []
        for(let i in cart.items){
            if(!cart.items[i])
                continue
            const jsx = (
                <tr key={cart.items[i].id}>
                    <td className="text-dark">{cart.items[i].name}</td>
                    <td className="text-dark">{cart.items[i].price}</td>

                    <td className="text-dark">{cart.items[i].beautician}</td>
                    <td className="text-dark">{cart.items[i].refered}</td>
                    <td>
                        <button type="button" disabled={this.state.btnDisabled} className="btn btn-danger m-2" onClick={this.updateCart} id={cart.items[i].id}>X</button>
                    </td>
                </tr>
            )
            cartBlock.push(jsx)
        }
        const jsx = (
            <tr key="total">
                <td><strong>Total</strong></td>
                <td className="text-dark"><strong>{cart.total}</strong></td>
            </tr>
        )
        cartBlock.push(jsx)
        return cartBlock
    }
    submit = (e) => {
        e.preventDefault();
        this.setState({
            btnDisabled : true
        })
        // console.log(this.state)
        this.setState({
            error : ''
        })
        if(!this.state.cart.items || this.state.cart.total === 0 ||  !this.state.cart.total){
            return this.setState({
                error : 'CART IS EMPTY!',
                btnDisabled : false
            })
        }
        if (!this.state.paymentType){
            return this.setState({
                error : 'PLEASE SELECT A PAYMENT TYPE!',
                btnDisabled : false
            })
        }
        this.props.middleware.makePayment({
            username:this.state.username,
            discount:this.state.discountAmount,
            paymentType : this.state.paymentType
        },(response) => {
            // console.log(response)
            if(!response.code){
                this.setState({
                    error : (<strong className="text-success">TRANSATION COMPLETED SUCCESFULLY</strong>),
                    recieptId : response.data.recieptId,
                    username : '',
                    cart : {},
                    cartBlock : "",
                    customerInfo : "",
                    btnDisabled : false
                })
                const date = new Date()
                this.pri.document.write("<div align='center'>Customer name: <strong>"+this.state.user.displayName+"</strong></div><hr/>")
                this.pri.document.write("<strong><p>Reciept Id: "+response.data.recieptId+"</p>")
                this.pri.document.write("<p align='right'>Date: "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()+"</p><p>Beautician: "+(this.state.beauticianName?this.state.beauticianName:'---')+"</p></strong>")

                var content = document.getElementById("cart");
                
                content.children[0].innerHTML += "<tr><td><strong>Discount</strong></td><td><strong>"+this.state.discountAmount+"</strong></td></tr>"
                content.children[0].innerHTML += "<tr><td><strong>PAID</strong></td><td><strong>"+this.state.payable+"</strong></td></tr>"
                this.pri.document.write(content.outerHTML);
                this.pri.document.write("<style>tr{border:2px solid grey}</style><hr/>")
                this.pri.document.write("<h5 align='right'>SIGNATURE<br/>("+this.props.middleware.state.user.displayName+")</h5>")

                this.pri.document.write("</div>")
                this.pri.document.close();
                // this.pri.focus();
                // this.pri.print();
                // this.pri.print();
                
            }
            else this.setState({
                error : "TRANSATION FAILED",
                username : '',
                cart : {},
                cartBlock : "",
                customerInfo : "",
                btnDisabled : false
            })
        })
    }
    render() {  
        return(
            <Presentation 
                {...this.state}
                update = {this.update}
                submit = {this.submit}
            />
        );
    }
}
export default Container;