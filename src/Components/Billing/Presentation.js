import React from 'react';
import {Route,Redirect,Switch} from 'react-router-dom'
// import GoogleAd from '../GoogleAd'

import ADD from './ADD'
import VIEW from './VIEW'
import EDIT from './PAYMENT'

class Presentation extends React.Component {

    render(){
        let action = this.props.middleware.state.user.services
        // console.log(action)
        for(let i in action)
            if(i === "BILLING")
            return (
                <div>
                    <Switch>
                        <Route path="/billing/add" component={() => <ADD middleware={this.props.middleware}/>}/>
                        <Route path="/billing/view" component={() => <VIEW middleware={this.props.middleware}/>}/>
                        <Route Path="/billing/payment" component={() => <EDIT middleware={this.props.middleware}/>}/>
                        <Redirect from="**" to="/billing/add"/>
                    </Switch>
                </div>
            )
            return (
                <h1>404</h1>
            )
                
        
    }
}

export default Presentation;