import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        cart : {},
        level : {},
        cartBlock: <tr><td>NO ITEMS IN CART</td></tr>,
        beautician : ""
    }

    componentDidMount(){
        this.pri = document.getElementById("ifmcontentstoprint").contentWindow;
        
        // this.pri.document.open()
        
        this.props.middleware.getItems((response) => {
            console.log(response)
            if(!response.code){
                this.setState({
                    data : response.data,
                    db : this.props.middleware.state.database
                })
                const parents = response.data.filter(item => item.parent === "").map((item) => {
                    // console.log(item)
                    // const e = <div></div>
                    return <button type="button" className="btn btn-dark m-2 btn-lg" key={item.id} onClick={this.laodChild} id={item.id} name="0">{item.name}</button>
                }) 
                const list = [<div className="container border">{parents}</div>]
                this.setState({
                    list : list
                })
                // console.log(this.state)
            }
        })
        this.props.middleware.getUsers((response) => {
            if (!response.code) {
                // console.log(response.data)
                let beauticians = response.data.filter(user => user.role === "BEAUTICIAN").map(user => {
                    return <option key={user.uid} value={user.uid}>{user.displayName + " (" + user.phoneNumber + ")"}</option>
                })
                beauticians = [<option key="" value="">None</option>, ...beauticians]
                this.setState({
                    beauticians: beauticians
                })
            }
        })
    }
    validate = (e) => {
        this.pri.document.open()
        if(this.pri.document.body)
            this.pri.document.body.innerText = ""
        // this.pri.document.clear()
        this.pri.document.write("<div style='border:1px solid black;padding:5%;'>")
        this.pri.document.write("<div align='center'><img alt='logo' height='50px' src='https://bit.ly/2ms5Krk'/></div>")

        this.pri.document.write("<div align='center'><stong>ESTIMATION SLIP</strong></div><hr/>")
        this.setState({
            printHeaderReady : true
        })
        console.log(e)
        if(e.target.name === "username"){
            e.target.value = "+91" + e.target.value
            this.setState({
                [e.target.name]: e.target.value
            })
            if(!e.target.value.match(/^\+[0-9]{12}$/)){
                return this.setState({
                    error : "INVALID MOBILE NUMBER",
                    parents : ''
                })
            }else{
                this.props.middleware.getUid(e.target.value,(response) => {
                    console.log(response)
                    if(!response.code && response.data){
                        const data = response.data
                        this.state.db.ref('/'+data.uid).once('value').then(res => {
                            const val = res.val()
                            if(val)
                                this.setState({
                                    cart : val,
                                    cartBlock : this.showCart(val)
                                })
                            else
                                this.setState({
                                    cart : {items : {},total : ''},
                                    cartBlock : <tr><td>NO ITEMS IN CART</td></tr>
                                })
                        })
                        this.setState({
                            customerInfo : (
                                <div className="bg-white">
                                    <div><strong>Name : </strong>{data.displayName}</div>
                                    <div><strong>Address : </strong>{data.address}</div>
                                    <div><strong>Email : </strong>{data.email}</div>
                                </div>
                            ),
                            user : response.data,
                            parents : this.state.list[0]
                        })
                    }else{
                        this.setState({
                            cart : "",
                            cartBlock : "",
                            customerInfo : "",
                            error : "INVALID MOBILE NUMBER",
                            parents : ''
                        })
                    }
                })
            }
        }
        if (e.target.name === "refered") {
            e.target.value = "+91" + e.target.value
            this.setState({
                [e.target.name] : e.target.value
            })
            if (!e.target.value.match(/^\+[0-9]{12}$/)) {
                return this.setState({
                    error: "INVALID MOBILE NUMBER"
                })
            } else {
                this.props.middleware.getUid(e.target.value, (response) => {
                    // console.log(response)
                    if (!response.code && response.data) {
                        const data = response.data
                        this.setState({
                            referedInfo: (
                                <div className="bg-white">
                                    <div><strong>Name : </strong>{data.displayName}</div>
                                    <div><strong>Address : </strong>{data.address}</div>
                                    <div><strong>Email : </strong>{data.email}</div>
                                </div>
                            ),
                            referedUser: response.data
                        })
                    } else {
                        this.setState({
                            referedInfo: "",
                            error: "INVALID MOBILE NUMBER"
                        })
                    }
                })
            }
        }
        if (e.target.name === "beautician") {
            return this.props.middleware.getUid(e.target.value, (response) => {
                if (!response.code) {
                    const data = response.data
                    if (e.target.name === 'beautician') {
                        return this.setState({
                            beauticianInfo: (
                                <div className="bg-white">
                                    <div><strong>Name : </strong>{data.displayName}</div>
                                    <div><strong>Address : </strong>{data.address}</div>
                                    <div><strong>Email : </strong>{data.email}</div>
                                </div>
                            ),
                            beauticianUser : data,
                            beauticianName: data.displayName,
                            error: ''
                        })
                    }
                }
            });
        }
        
    }

    update = (e) => {
        this.setState({
            [e.target.name] : e.target.value,
            error : ''
        })
        this.validate({target : { name : e.target.name,value : e.target.value}})
        if((e.target.name === "beautician" && !e.target.value) || !this.state.beautician) {
            return this.setState({
                error: "It's important to select an beautician",
            })
        }
    }
    showCart = (cart) => {
        // console.log(cart)
        const cartBlock = []
        for(let i in cart.items){
            if(!cart.items[i])
                continue
            const jsx = (
                <tr key={cart.items[i].id}>
                    <td>{cart.items[i].name}</td>
                    <td>{cart.items[i].price}</td>
                    <td>{cart.items[i].beautician || ""}</td>
                </tr>
            )
            cartBlock.push(jsx)
        }
        const jsx = (
            <tr key="total">
                <td><strong>Total</strong></td>
                <td className="text-dark"><strong>{cart.total}</strong></td>
            </tr>
        )
        cartBlock.push(jsx)
        return cartBlock
    }

    laodChild = e => {
        console.log(this.state)
        if(this.state.data.filter(item => item.parent === e.target.id).length === 0){
            let cart = this.state.cart.items
            cart[e.target.id] = cart[e.target.id] ? false : this.state.data.filter(item => item.id === e.target.id)[0]
            if(cart[e.target.id]){
                cart[e.target.id].beautician = this.state.beautician || ""
                cart[e.target.id].refered = this.state.refered || ""
            }
            // console.log(cart)
            let total = 0
            for(let i in cart){
                if(cart[i].price)
                    total += parseInt(cart[i].price)
            }
            this.setState({
                totalCost : total
            })

            this.pri.document.open()
            if (this.pri.document.body)
                this.pri.document.body.innerText = ""
            // this.pri.document.clear()
            this.pri.document.write("<div style='border:1px solid black;padding:5%;'>")
            this.pri.document.write("<div align='center'><img alt='logo' height='50px' src='https://bit.ly/2ms5Krk'/></div>")

            this.pri.document.write("<div align='center'><stong>ESTIMATION SLIP</strong></div><hr/>")
            this.setState({
                printHeaderReady : true
            })
            console.log(cart)
            cart = {items : cart,total : total}
            this.state.db.ref('/'+this.state.user.uid).set(
                {...cart}
            )
            console.log(cart)
            return this.setState({
                cart : cart,
                cartBlock : this.showCart(cart)
            })
        }
        const level = this.state.level
        
        // console.log(this.state)
        if(level[e.target.name])
            if(level[e.target.name][e.target.id] !== null){
                let old = [...this.state.list]
                // old.pop()
                const list = []
                for(let i=0;i<parseInt(e.target.name)+1;i++){
                    list.push(old[i])
                }
                let c = parseInt(e.target.name)
                while(level[c]){
                    level[c]  = null
                    c+=1
                }
                old = [...list]
                // level[e.target.name] = null
                console.log(this.state)
                return this.setState({
                    list : old,
                    parents : old[old.length-1]
                })
            }
        level[e.target.name] = {[e.target.id]:true}
        this.setState({
            level : level
        })
        const childs = this.state.data.filter(item => item.parent === e.target.id).map(item => {
            return <button type="button" className="btn btn-info m-1 btn-lg" key={item.id} onClick={this.laodChild} name={parseInt(e.target.name)+1} id={item.id}>{item.name}</button>
        })
        let old = [...this.state.list]
        // old.pop()
        const list = []
        for (let i = 0; i < parseInt(e.target.name) + 1; i++) {
            list.push(old[i])
        }
        list.push(<div className="border">{list[list.length-1]}<div className="container">{childs}</div></div>)
        // console.log(list)
        this.setState({
            list : list,
            parents : list[list.length-1]
        })
        
    }

    print = () => {
        if(!this.state.cart.items || !this.state.cart.total)
            return;
        if(!this.state.printHeaderReady){
            this.pri.focus()
            return this.pri.print()
        }
        this.setState({
            printHeaderReady : false
        })
        const date = new Date()
        this.pri.document.write("<div align='center'>Customer name: <strong>"+this.state.user.displayName+"</strong></div><hr/>")
        this.pri.document.write("<h4 align='right'>Date : "+date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()+"</h4>")

        var content = document.getElementById("cart");
        this.pri.document.write(content.outerHTML);
        this.pri.document.write("<style>tr{border:2px solid grey}</style><hr/>")
        this.pri.document.write("<h5 align='right'>SIGNATURE<br/>("+this.props.middleware.state.user.displayName+")</h5>")

        this.pri.document.write("</div>")
        this.pri.document.close()
        this.pri.focus();
        this.pri.print();
    }
    submit = (e) => {
        e.preventDefault();
    }

    render() {  
        return(
            <Presentation 
                {...this.state}
                update = {this.update}
                print = {this.print}
                submit={this.submit}
            />
        );
    }
}
export default Container;