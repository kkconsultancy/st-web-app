import React from 'react';

class Presentation extends React.Component {
    render(){
        return (
            
            
<form onSubmit={this.props.submit} className="mt-2 container text-center">
	<iframe id="ifmcontentstoprint" title="ifmcontentstoprint" style={{height: "0px",width: "0px",position: "absolute",display:"none"}}></iframe>
	<div className="card1">
		<div className="card-body" >
			<h4 className="card-title text-dark">ADD ITEMS TO CART</h4>
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<div className="row  mt-3 mb-3 form">
				{/* <div className="col-0 col-lg-1"/> */}
				<div className="col-2">
					<label>Customer Mobile No.</label>
				</div>
				<div className="col-4 input-group">
					<div class="input-group-text">
						<div class="input-group-text">+91</div>
						<input type="text" disabled={this.props.btnDisabled} className="form-control" name="username" placeholder="9999999990" onChange={this.props.update} />
					</div>
				</div>
				<div className="col border-2">
                    {this.props.customerInfo}
                </div>
			</div>
			<div className="row mt-3 mb-3 form-group">
				{/* <div className="col-0 col-lg-1" /> */}
				<div className="col-2">
					<label>Beautician</label>
				</div>
				<div className="col-4">
					<select disabled={this.props.btnDisabled} className="form-control" name="beautician" onChange={this.props.update}>
						{this.props.beauticians}
					</select>
				</div>
				<div className="col border-2">
					{this.props.beauticianInfo}
				</div>
			</div>
			<div className="row  mt-3 mb-3 form">
				{/* <div className="col-0 col-lg-1"/> */}
				<div className="col-2">
					<label>Reference Mobile No.</label>
				</div>
				<div className="col-4 input-group">
					<div class="input-group-text">
						<div class="input-group-text">+91</div>
						<input type="text" disabled={this.props.btnDisabled} className="form-control" name="refered" placeholder="9999999990" onChange={this.props.update} />
					</div>
				</div>
				<div className="col border-2">
					{this.props.referedInfo}
				</div>
			</div>
		</div>

	<div className="car">
		<div className="card-body mb-5">
			<div className="row mt-5 mb-5 form-group">
				<div className="col-12 col-lg-12">
                                {this.props.parents}
                            </div>
				<div className="col"style={{margin:"10px"}}>
					<div className="row mb-3 form-group">
						<div className="col-3"/>
						<div className="col-6">
							<button className="btn btn-outline-primary btn-sm active mt-5" type="button" onClick={this.props.print}>PRINT SAMPLE</button>
							<table width="100%" id="cart" className="border">
								<tbody>
									<tr>
										<th>NAME</th>
										<th>PRICE</th>
										<th>BEAUTICIAN</th>
									</tr>

									{this.props.cartBlock}

								</tbody>
							</table>
						</div>
						<div className="col-3"/></div>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
                
        )
    }
}

export default Presentation;