import React from 'react';
import './style.css'
class Presentation extends React.Component {
    
    render(){
      return (
          <div className="container my-5">
              <form className="container " onSubmit={this.props.submit}>

                    <div class="cards card-3s">
                        <div class="appointment-img"></div>
                        <div class="card-body text-center" >
                            <h2 class="title" id="section1 ml-5">
                                {/* Make an Appointment */}
                                {/* <NavLink to="/" className="FormField__Link">SignUp</NavLink> */}
                            </h2>
                            <div class="textlogin">
                            <div className="card-header text-danger">
                                {this.props.error}
                            </div>

                            <div className="row input-group">
                                <div class="input-group-text">+91</div>
                                <input class="form-control" type="text"  placeholder="Enter the Mobile Number"  onChange={this.props.updation} name="mobilenumber" />
                            </div>
                            <div class="row input-group">
                                <input class="form-control" type="text" placeholder="Enter the Name"  onChange={this.props.updation}  name="name"/>
                            </div>
                            <div class="row input-group">
                                <input type="date" placeholder="Select Appointment Date" className="form-control" id="pickup_date"  onChange={this.props.updation} name="date"/>    
                            </div>
                            <div class="row input-group">                                      
                                <select className="form-control" onChange={this.props.updation}  name="location">
                                    <option selected="selected">Please Select Branch</option>
                                    <option value="Kakinada-I">Kakinada-I</option>
                                    <option value="Kakinada-II">Kakinada-II</option>
                                    <option value="Rajamandry">Rajamandry</option>
                                </select>
                            </div>
                            <div class="row input-group">                                
                                <select className="form-control"  onChange={this.props.updation} name="gender">
                                    <option disabled="disabled" selected="selected" >Please Select Gender</option>
                                    <option value="MALE">Male</option>
                                    <option value="FEMALE">Female</option>
                                </select>
                            </div>
                            <div class="row input-group">
                                <select className="form-control"  onChange={this.props.updation} name="time" >
                                    <option disabled="disabled" selected="selected" >Please Select Schedule Time</option>
                                    <option value="09:00 AM">09:00 AM</option>
                                    <option value="09:30 AM">09:30 AM</option>
                                    <option value="10:00 AM">10:00 AM</option>
                                    <option value="10:30 AM">10:30 AM</option>
                                    <option value="11:00 AM">11:00 AM</option>
                                    <option value="11:30 AM">11:30 AM</option>
                                    <option value="12:00 PM">12:00 PM</option>
                                    <option value="12:30 PM">12:30 PM</option>
                                    <option value="01:00 PM">01:00 PM</option>
                                    <option value="01:30 PM">01:30 PM</option>
                                    <option value="02:00 PM">02:00 PM</option>
                                    <option value="02:30 PM">02:30 PM</option>
                                    <option value="03:00 PM">03:00 PM</option>
                                    <option value="03:30 PM">03:30 PM</option>
                                    <option value="04:00 PM">04:00 PM</option>
                                    <option value="04:30 PM">04:30 PM</option>
                                    <option value="05:00 PM">05:00 PM</option>
                                    <option value="05:30 PM">05:30 PM</option>
                                    <option value="06:00 PM">06:00 PM</option>
                                    <option value="06:30 PM">06:30 PM</option>
                                    <option value="07:00 PM">07:00 PM</option>
                                    <option value="07:30 PM">07:30 PM</option>
                                    <option value="08:00 PM">08:00 PM</option>
                                </select>
                            </div><br></br>
                            <button  class="btn-lg mb-5 text-white buttons" type="submit" onClick={this.props.submit}>Submit</button>
                             </div>    
                           
                        </div>
                    </div>

              </form>
              
   
             
          </div>   
        )
    }
}

export default Presentation;