import React, {Component} from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        mobilenumber : '' ,
        name : '',
        date : '' ,
        location : '',
        gender : '',
        time : ''
    }
    
    callback = (action) => {
        console.log(action)
        if(action.code){
            this.setState({
                error : action.message
            })
        }else{
            this.setState({
                error : (<div className={"text-success"}>{"APPOINTMENT SUCCESFULLY CREATED FOR "+this.state.date+" @"+this.state.time}</div>)
            })
        }
    }
    submit = (e) => {
        e.preventDefault();
        if(this.state.error)
            return;
        for(let i in this.state){
            if(!this.validate({
                target : {
                    name : i,
                    value : this.state[i]
                }
            })) return;
        }
        // console.log(this.state);
        const data = {
          phoneNumber : "+91"+this.state.mobilenumber,
          displayName : this.state.name,
          date : this.state.date,
          time : this.state.time,
          timestamp : new Date().toUTCString(),
          gender : this.state.gender,
          branch : this.state.location
        }
        console.log(data)
        this.props.middleware.createAppointment(data,this.callback)
    }
    validate = (e) => {
        console.log(e)
        if(e.target.name==='name'){
            let reg=/^[A-z\s]{3,}$/
            if(!e.target.value.match(reg) || e.target.value === ""){
                this.setState({
                    error : 'USER IS INVALID MIN 5 CHARACTERS!'
                })
                return false
            }
        }
        else if(e.target.name==='mobilenumber'){
            let reg=/^[0-9]{10}$/
            // let reg=/^\+[0-9]{12}$/
            // console.log(e.target.value.match(reg))
            if(!e.target.value.match(reg) || e.target.value === ""){
                this.setState({
                    error : "MOBILE NO. IS INVALID!"
                })
                return false
            }
        }
        else if(e.target.name==='date'){
            if(!e.target.value || e.target.value === ""){
                this.setState({
                    error : 'invalid date'
                })
                return false
            }
        }else if(e.target.name==='location'){
          if(!e.target.value || e.target.value === ""){
            this.setState({
                error : 'SELECT THE LOCATION'
            })
            return false
          } 
        }
        else if(e.target.name==='gender'){
          if(!e.target.value || e.target.value === ""){
            this.setState({
                error : 'SELECT THE GENDER'
            })
            return false
          } 
        }else if(e.target.name==='time'){
          if(!e.target.value || e.target.value === ""){
            this.setState({
                error : 'SELECT THE TIME'
            })
            return false
          } 
        }
        return true
    }

    updation = (e) => {
        this.setState({
            [e.target.name] : e.target.value ,
            error:''
        })
        this.validate({
            target:{
                name : e.target.name,
                value : e.target.value
            }
        })
    }

    render() {

        return(
            <Presentation
            {...this.state}
            User = {this.User}
            updation = {this.updation}
            submit = {this.submit}
            />
        );
    }
}
export default Container;