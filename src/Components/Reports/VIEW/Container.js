import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {}
    componentDidMount(){
        this.pri = document.getElementById("ifmcontentstoprint").contentWindow;
        this.pri.addEventListener('afterprint',this.setHeader()) 
        this.setHeader()
        this.props.middleware.reports.getReports(response => {
            if(response.code)
                return this.setState({
                    error : "UNABLE TO GET THE DATA"
                })
            // console.log(response)
            const data = response.transactions
            if(!data || data.length === 0){
                return this.setState({
                    error : "OOOPS NO TRANSACTIONS!!!"
                })
            }
            // console.log(data)
            this.setState({
                data : data
            })
            const table = []
            for(let i in data){
                console.log(data[i])
                const divs = []
                for(let k in data[i].items){
                    // console.log(k)
                    if(data[i].items[k]){
                        // console.log(data[i].items)
                        const jsx = (
                            <div key={k}>
                                {data[i].items[k].name}(<strong>{data[i].items[k].beautician}</strong>)
                            </div>
                        )
                        divs.push(jsx)
                    }
                }
                // console.log(divs)
                    const jsx = (
                        <tr id={i} key={i}>
                            {/* <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td> */}
                            <td>{data[i].transactionNumber}</td>
                            <td>{data[i].operatorId}</td>
                            <td>{data[i].customerId}</td>
                            <td>{divs}</td>
                            <td>{data[i].total}</td>
                            <td>{data[i].discount}</td>
                            <td>{data[i].payment}</td>
                            <td>{data[i].paymentType}</td>
                            <td>{data[i].timestamp}</td>
                        </tr>
                    )
                    table.push(jsx)
            }
            this.setState({
                transactionsToday : table,
                transactionsData: data
            })
        })

    }
    setHeader = () => {
        this.pri.document.open()
        this.pri.document.clear()
        this.pri.document.write("<div style='border:1px solid black;padding:5%;'>")
        this.pri.document.write("<div align='center'><img alt='logo' height='50px' src='https://bit.ly/2ms5Krk'/></div>")
        this.pri.document.write("<div align='center'><stong>BILL</strong></div><hr/>")
    }
    printBill = (e) => {
        const data = this.state.data[e.target.value]

        this.pri.document.write("<div align='center'>Customer: <strong>"+data.customerId+"</strong></div><hr/>")

        this.pri.document.write("<strong><p>Reciept Id: "+data.transactionNumber+"</p>")
        this.pri.document.write("<p align='right'>Date: "+data.date.day+"-"+(parseInt(data.date.month)+1)+"-"+data.date.year+"</p><p>Beautician: "+(data.beauticianId?data.beauticianId:'---')+"</p></strong>")

        var content = '<table width="100%"><tbody><tr><th>NAME</th><th>COST</th></tr>'
        // console.log(data.items)
        for(let i in data.items){
            const item = data.items[i]
            if(item)
                content += "<tr><td>"+item.name+"</td><td>"+item.price+"</td></tr>"
        }
        content += "<tr><td><strong>Total</strong></td><td><strong>"+data.total+"</strong></td></tr><hr>"
        content += "<tr><td><strong>Discount</strong></td><td><strong>"+data.discount+"</strong></td></tr>"
        content += "<tr><td><strong>PAID</strong></td><td><strong>"+data.payment+"</strong></td></tr>"
        content += '</tbody></table>'
        this.pri.document.write(content)
        this.pri.document.write("<style>tr{border:2px solid grey}</style><hr/>")
        this.pri.document.write("<h5 align='right'>SIGNATURE<br/>("+data.operatorId+")</h5>")

        this.pri.document.write("</div>")

        this.pri.document.close()
        this.pri.focus();
        this.pri.print();
        this.pri.document.clear()
    }

    search = (e) => {

        let table = []
        if (e.target.value && e.target.value.match(/.*/)) {
            if(e.target.value.match(/\+/))
                return;
            const query = e.target.value.toLowerCase()
            try{
                RegExp(query)
            }catch(err){
                return console.log(err)
            }
            table = this.state.transactionsData.filter(transaction => {
                if(!transaction)
                    return false
                if (transaction.customerId.toLowerCase().match(query))
                    return true
                if (transaction.operatorId.toLowerCase().match(query))
                    return true
                if ((transaction.transactionNumber+"").toLowerCase().match(query))
                    return true
                if ((transaction.paymentType + "").toLowerCase().match(query))
                    return true

                const items = transaction.items.filter(item => item).filter(item => item.name.toLowerCase().match(query) || item.description.toLowerCase().match(query))
                if(items.length > 0)
                    return true

                if (transaction.timestamp.toLowerCase().match(query))
                    return true
                return false
            }).map(data => {
                const divs = []
                for (let k in data.items) {
                    // console.log(k)
                    if (data.items[k]) {
                        // console.log(data.items,k)
                        const jsx = (
                            <div key={k}>
                                {data.items[k].name}({data.items[k].beautician})
                            </div>
                        )
                        divs.push(jsx)
                    }
                }
                return (
                    <tr id={data.transactionNumber} key={data.transactionNumber}>
                        {/* <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td> */}
                        <td>{data.transactionNumber}</td>
                        <td>{data.operatorId}</td>
                        <td>{data.customerId}</td>
                        <td>{divs}</td>
                        <td>{data.total}</td>
                        <td>{data.discount}</td>
                        <td>{data.payment}</td>
                        <td>{data.paymentType}</td>
                        <td>{data.timestamp}</td>
                    </tr>
                )
            })
        }
        else {
            table = this.state.transactionsData.map(data => {
                const divs = []
                for (let k in data.items) {
                    // console.log(k)
                    if (data.items[k]) {
                        const jsx = (
                            <div key={k}>
                                {data.items[k].name}({data.items[k].beautician})
                            </div>
                        )
                        divs.push(jsx)
                    }
                }
                return (
                    <tr id={data.transactionNumber} key={data.transactionNumber}>
                        {/* <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td> */}
                        <td>{data.transactionNumber}</td>
                        <td>{data.operatorId}</td>
                        <td>{data.customerId}</td>
                        <td>{divs}</td>
                        <td>{data.total}</td>
                        <td>{data.discount}</td>
                        <td>{data.payment}</td>
                        <td>{data.paymentType}</td>
                        <td>{data.timestamp}</td>
                    </tr>
                )
            })
        }
        this.setState({
            transactionsToday: table,
        })
    }

    render() {  
        return(
            <Presentation 
                {...this.state}
                search={this.search}
            />
        );
    }
}
export default Container;