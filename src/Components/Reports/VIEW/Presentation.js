import React from 'react';

class Presentation extends React.Component {
    render(){
        return (
            
<form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
	<iframe id="ifmcontentstoprint" title="ifmcontentstoprint" style={{height: "0px",width: "0px",position: "absolute",display:"none"}}></iframe>
	<div className="card mt-5 mb-5">
		<div className="card-body">
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<h4 className="card-title text-dark">REPORTS</h4>
			<div className="row mt-1 mb-1 form-group">
				<div className="col col-lg-8" />
				<div className="col col-lg-4">
					<div className="input-group">
						<div className="input-group-prepend">
							<span className="input-group-text mt-5">@</span>
						</div>
						<input className="btn-outline-secondary btn form-control mt-5" onChange={this.props.search} type="text" placeholder="details" />
					</div>
				</div>
			</div>
			<div className="row mt-3 mb-3 form-group">
			
				<div className="col-12">
					<table width="100%" border="1">
						<tbody>
							<tr>
								<th>TRANSACTION</th>
								<th>OPERATOR</th>
								<th>CUSTOMER</th>
								<th>ITEMS(BEAUTICIAN)</th>
								<th>TOTAL COST</th>
								<th>DISCOUNT</th>
								<th>PAID</th>
								<th>PAYMENT TYPE</th>
								<th>TIME</th>
							</tr>
                            {this.props.transactionsToday}
                                
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</form>
        )
    }
}

export default Presentation;