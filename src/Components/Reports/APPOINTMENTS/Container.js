import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {}
    componentDidMount(){
        const date = new Date().toISOString().substring(0,10);

        const data = {
            date : date
        }
        this.props.middleware.reports.getAppointments(data,response => {
            if(response.code)
                return this.setState({
                    error : "UNABLE TO GET THE DATA"
                })
            // console.log(response)
            const data = response.appointments
            // console.log(data)
            if(!data || data.length === 0){
                return this.setState({
                    error : "OOOPS NO APPOINTMENTS!!!"
                })
            }
            // console.log(data)
            this.setState({
                data : data
            })
            const table = []
            for(let i in data){
                // console.log(data[i])
                    const jsx = (
                        <tr id={i} key={i}>
                            {/* <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td> */}
                            <td>{data[i].displayName}</td>
                            <td>{data[i].branch}</td>
                            <td>{data[i].gender}</td>
                            {/* <td>{divs}</td> */}
                            <td>{data[i].uid}</td>
                            <td>{data[i].appointment.date}</td>
                            <td>{data[i].appointment.time}</td>
                            {/* <td>{data[i].timestamp}</td> */}
                        </tr>
                    )
                    table.push(jsx)
            }
            this.setState({
                appointmentsToday : table,
                appointmentsData: data
            })
        })

    }

    search = (e) => {

        let table = []
        if (e.target.value && e.target.value.match(/.*/)) {
            if(e.target.value.match(/\+/))
                return;
            const query = e.target.value.toLowerCase()
            table = this.state.appointmentsData.filter(appointment => {
                if (appointment.displayName.toLowerCase().match(query))
                    return true
                if (appointment.uid.toLowerCase().match(query))
                    return true
                return false
            }).map(data => {
                const jsx = (
                    <tr id={data.timestamp} key={data.timestamp}>
                        {/* <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td> */}
                        <td>{data.displayName}</td>
                        <td>{data.branch}</td>
                        <td>{data.gender}</td>
                        {/* <td>{divs}</td> */}
                        <td>{data.uid}</td>
                        <td>{data.appointment.date}</td>
                        <td>{data.appointment.time}</td>
                        {/* <td>{data[i].timestamp}</td> */}
                    </tr>
                )
                return jsx
            })
        }
        else {
            table = this.state.transactionsData.map(data => {
                const jsx = (
                    <tr id={data.timestamp} key={data.timestamp}>
                        {/* <td><button className="btn btn-light" type="button" value={i} onClick={this.printBill}>PRINT</button></td> */}
                        <td>{data.displayName}</td>
                        <td>{data.branch}</td>
                        <td>{data.gender}</td>
                        {/* <td>{divs}</td> */}
                        <td>{data.uid}</td>
                        <td>{data.appointment.date}</td>
                        <td>{data.appointment.time}</td>
                        {/* <td>{data[i].timestamp}</td> */}
                    </tr>
                )
                return jsx
            })
        }
        this.setState({
            transactionsToday: table,
        })
    }

    render() {  
        return(
            <Presentation 
                {...this.state}
                search={this.search}
            />
        );
    }
}
export default Container;