import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import { DatePicker } from "antd";



class Presentation extends React.Component {
	state = {
		startValue: null,
		endValue: null,
		endOpen: false
	  };
	
	  disabledStartDate = startValue => {
		const { endValue } = this.state;
		if (!startValue || !endValue) {
		  return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	  };
	
	  disabledEndDate = endValue => {
		const { startValue } = this.state;
		if (!endValue || !startValue) {
		  return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	  };
	
	  onChange = (field, value) => {
		this.setState({
		  [field]: value
		});
	  };
	
	  onStartChange = value => {
		this.onChange("startValue", value);
	  };
	
	  onEndChange = value => {
		this.onChange("endValue", value);
	  };
	
	  handleStartOpenChange = open => {
		if (!open) {
		  this.setState({ endOpen: true });
		}
	  };
	
	  handleEndOpenChange = open => {
		this.setState({ endOpen: open });
	  };
	
	render() {
		const { startValue, endValue, endOpen } = this.state;
        return (
            
<form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
	<iframe id="ifmcontentstoprint" title="ifmcontentstoprint" style={{height: "0px",width: "0px",position: "absolute",display:"none"}}></iframe>
	<div className="card mt-5 mb-5">
		<div className="card-body">
			<div className="card-header text-danger">
                        {this.props.error}
                        {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    
			</div>
			<h4 className="card-title text-dark">APPOITMENTS</h4>
				<div className="row justify-content-between mt-1 mb-1 form-group">
							
								
					<div className="col col-lg-4">			
							<div className="input-group">									
								<div className="input-group-prepend">
									<span className="input-group-text mt-5">@</span>
								</div>
								<input className="btn-outline-secondary btn form-control mt-5" onChange={this.props.search} type="text" placeholder="details" />
								</div>
								
							</div>
							<div className="row mt-5 mr-3 justify-content-around">
								<div className="col-x-4 mr-2">
								<DatePicker disabledDate={this.disabledStartDate}showTime format="YYYY-MM-DD HH:mm:ss" value={startValue} placeholder="Start" onChange={this.onStartChange} onOpenChange={this.handleStartOpenChange}/>
        						<DatePicker disabledDate={this.disabledEndDate}showTime format="YYYY-MM-DD HH:mm:ss" value={endValue} placeholder="End" onChange={this.onEndChange} open={endOpen} onOpenChange={this.handleEndOpenChange} />
								</div>								
								
							</div>
								
			</div>
			<div className="row mt-3 mb-3 form-group">
			
				<div className="col-12">
					<table width="100%" border="1">
						<tbody>
							<tr>
								<th>Name</th>
								<th>Branch</th>
								<th>Gender</th>
								<th>Mobile Number</th>
								<th>Date</th>
								<th>Time</th>
							</tr>
                            {this.props.appointmentsToday}
                                
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</form>
        )
    }
}

export default Presentation;