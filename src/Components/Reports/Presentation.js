import React from 'react';
import {Route,Redirect,Switch} from 'react-router-dom'
// import GoogleAd from '../GoogleAd'

import VIEW from './VIEW'
import APPOINTMENTS from './APPOINTMENTS'

class Presentation extends React.Component {

    render(){
        let action = this.props.middleware.state.user.services
        // console.log(action)
        for(let i in action)
            if(i === "REPORTS")
            return (
                <div>
                    <Switch>
                      <Route path="/reports/view" component={() => <VIEW middleware={this.props.middleware}/>}/>
                      <Route path="/reports/appointments" component={() => <APPOINTMENTS middleware={this.props.middleware}/>}/>
                      <Redirect from="**" to="/reports/view"/>
                  </Switch>
                </div>
            )
            return (
                <img src="/images/404.png" alt="404"/>
            )
        
    }
}

export default Presentation;