import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    callback = (action) => {
        console.log(action)
        if(action.code){
            this.setState({
                error : action.message,
                btnDisabled : false
            })
        }
    }
    state = {
        email : '',
        password : '',
        btnDisabled : false,
        user : '',
        error : ''
    }
    signin = (e) => {
        e.preventDefault();
        if(!this.state.email || !this.state.password){
            return;
        }
        this.setState({
            btnDisabled : true
        })
        let data = {
            email : this.state.email,
            password : this.state.password
        }
        if(!this.state.email.match(/^.+@.+\..+$/)){
            if(this.state.user.email)
                data.email = this.state.user.email
            else
                return this.setState({
                    error : "USER NOT FOUND",
                    btnDisabled : false
                })
        }
        // console.log(data)
        this.props.middleware.signin(data,this.callback)
    }
    update = (e) => {
        if(e.target.name === "email")
            this.setState({
                user : ''
            })
        if(this.state.email && e.target.name === "password"){
            this.props.middleware.getUid(this.state.email,(response) => {
                // console.log(response)
                if(response.data){
                    this.setState({
                        user : response.data
                    })
                }
            })
        }
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    render() {
        return(
            <Presentation
                {...this.state}
                signin = {this.signin}
                update = {this.update}            
            />
        );
    }
}
export default Container;