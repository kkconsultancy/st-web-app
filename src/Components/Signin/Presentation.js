import React from 'react';
import './index1.css';



class Presentation extends React.Component {
    
    render(){
      return (     
        <div class="text-center">
          <img id="banner-bg"  src='/images/logo.png' alt="logo" class="banner-logo " />
        <div class="container mt-5 text-center">
    <div class="row mt-5">
              <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
              <div className="card-headerx text-danger">
                {this.props.error}
                {/* <img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
              </div>
        <div class="cardh card-signin ">
          <div class="card-bodyx">
            <h5 class="card-title text-center">Login</h5>
            <form class="form-signin">
              <div class="form-label-group">
                <input type="email" disabled={this.props.btnDisabled} name="email" onChange={this.props.update} id="inputEmail" class="form-control" placeholder="Email address" required autofocus/>
                <label for="inputEmail">Email address</label>
              </div>

              <div class="form-label-group">
                <input type="password"  disabled={this.props.btnDisabled} name="password" onChange={this.props.update} id="inputPassword" class="form-control" placeholder="Password" required/>
                <label for="inputPassword">Password</label>
              </div>

              <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="customCheck1"/>
                <label class="custom-control-label" for="customCheck1">Remember password</label>
              </div>
              <button type="button"  disabled={this.props.btnDisabled} onClick={this.props.signin} class="btn btn-primary btn-lg mt-5">Login</button>
<button type="button"  style={{marginLeft:"22px"}} disabled={this.props.btnDisabled} class="btn btn-primary btn-lg mt-5">Reset</button>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
        
                
              
             
        )
    }
}

export default Presentation;