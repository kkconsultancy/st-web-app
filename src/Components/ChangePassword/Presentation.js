import React from 'react';
// import GoogleAd from '../GoogleAd'
class Presentation extends React.Component {
    
    render(){
        return (
          
<form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
	<div className="card0 mt-5 mb-5">
		<div className="card-body">
			<div className="card-header text-danger">
                {this.props.error}
                {/* 
				<img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
              
			</div>
			<h4 className="card-title text-dark">CHANGE PASSWORD</h4>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-2"/>
				<div className="col-4 col-lg-2">
					<label>Old Password</label>
				</div>
				<div className="col-8 col-lg-5">
					<input className="form-control" type="password" onChange={this.props.update} name="opassword" placeholder="********"/>
				</div>
				<div className="col-0 col-lg-3"/></div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-2"/>
				<div className="col-4 col-lg-2">
					<label>New Password</label>
				</div>
				<div className="col-8 col-lg-5">
					<input className="form-control" type="password" onChange={this.props.update} name="password" placeholder="********"/>
				</div>
				<div className="col-0 col-lg-3"/></div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-0 col-lg-2"/>
				<div className="col-4 col-lg-2">
					<label>Confirm New Password</label>
				</div>
				<div className="col-8 col-lg-5">
					<input className="form-control" type="password" onChange={this.props.update} name="cpassword" placeholder="********"/>
				</div>
				<div className="col-0 col-lg-3"/></div>
			<div className="row mt-3 mb-3 form-group">
				<div className="col-lg-4 col-2"/>
				<div className="col-lg-2 col-4">
					<button className="btn btn-outline-warning" type="submit" onClick={this.props.submit}>UPDATE</button>
				</div>
				<div className="col-lg-2 col-4">
					<button className="btn btn-outline-dark" type="reset" variant="primary" disabled={this.props.btnDisabled} >RESET</button>
				</div>
				<div className="col-lg-4 col-2"/></div>
		</div>
	</div>
</form>
        )
    }
}

export default Presentation;