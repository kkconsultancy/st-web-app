import React, {Component } from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        password : '',
        opassword : '',
        cpassword : '',
        error : ''
    }

    validate = () => {
        const password = this.state.password
        if(password){
            if(password.length < 6){
                this.setState({
                    error : "PASSWORD LENGTH SHOULD BE ATLEAST 6 CHARACTERS"
                })
                return false
            }
            if(password === this.state.opassword){
                this.setState({
                    error : "OLD AND NEW PASSWORD SHOULD BE DIFFERENT"
                })
                return false
            }
            if(password !== this.state.cpassword){
                this.setState({
                    error : "NEW AND CONFIRM PASSWORDS MUST MATCH"
                })
                return false
            }
        }
        return true
    }

    update = (e) => {
        // console.log(e.target.name +":"+e.target.value)
        this.setState({
            [e.target.name] : e.target.value,
            error : ""
        })
        this.validate()
    }

    submit = (e) => {
        e.preventDefault();
        this.setState({
            error : ''
        })
        if(!this.validate())
            return false
        const data = {
            password : this.state.password,
            opassword : this.state.opassword
        }
        this.props.middleware.changePassword(data,(response) => {
            console.log(response)
        })
    }

    render() {
        return(
            <Presentation
                {...this.state}
                update={this.update}
                submit={this.submit}
            />
        );
    }
}
export default Container;