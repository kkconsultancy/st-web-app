import React from 'react';



class Presentation extends React.Component {
    
    render(){
      return (
        
<div className="bann">
	<div className="">
		<div class="containerin navbarpage">
			<div class="brand">
				<div className="logobrand">
					<img alt="logo" style={{maxWidth:"100%",maxHeight:"20vh"}} src="https://bit.ly/2ms5Krk"/>
				</div>
			</div>
			<div class="hello">
				<div class=" dropdown">
					<a class="btn 	dropdown-toggle btn btn-primary " data-toggle="dropdown" href="0">HELLO
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="2">
								<span class="glyphicon glyphicon-user"></span> Logout
							
							</a>
						</li>
						<li>
							<a href="3">
								<span class="glyphicon glyphicon-log-in"></span> Change Password
							
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="containernavbarbutton">
		<div class="row">
			<div class="col-xs-8 col-md-0 col-lg-3 mt-1">
					<div className="btn-groups">
						<button onClick={this.props.handleMain1}id="main3" class="btn btn-dark btn-lg" data-toggle="collapse" data-target="#bu1">
							<h1>Billing</h1>
						</button>
						<div id="bu1" class="collapse">
							<div class="body1" id="body1">							
									<button class="btn  btn-warning btn-lg d-block ml-5 mt-2" data-target="#mah1" data-toggle="collapse">Add</button>															
									<button class="btn btn-success btn-lg  d-block ml-5 mt-2" data-target="#mah2" data-toggle="collapse">View</button>														
									<button class="btn btn-info btn-lg  d-block ml-5 mt-2" data-target="#mah3" data-toggle="collapse">Payment</button>							
							</div>
						</div>
					</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3 mt-1">
					<div className="btn-groups">
						<button onClick={this.props.handleMain2} id="main2" class="btn btn-dark btn-lg" data-toggle="collapse" data-target="#bu2">
							<h1>Catalogue</h1>
						</button>
						<div id="bu2" class="body2 collapse">
							<div class="body2" id="body2">
								<button class="btn btn-warning btn-lg d-block ml-5 mt-2" data-target="#mah1" data-toggle="collapse">Add</button>
								<button class="btn btn-success btn-lg d-block ml-5 mt-2" data-target="#mah2" data-toggle="collapse">View</button>
								<button class="btn btn-info btn-lg d-block ml-5 mt-2" data-target="#mah3" data-toggle="collapse">Remove</button>
							</div>
						</div>
					</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3 mt-1">
					<div className="btn-groups">
						<button onClick={this.props.handleMain3}id="main3" class="btn btn-dark btn-lg" data-toggle="collapse" data-target="#bu3">
							<h1>Users</h1>
						</button>
						<div id="bu3" class="collapse ">
							<div class="body3" id="body3">
								<button class="btn btn-warning btn-lg  d-block ml-5 mt-2" data-target="#mah1" data-toggle="collapse">Add</button>
								<button class="btn btn-success btn-lg   d-block ml-5 mt-2" data-target="#mah2" data-toggle="collapse">Update</button>
								<button class="btn btn-info btn-lg  d-block ml-5 mt-2" data-target="#mah3" data-toggle="collapse">View</button>
								<button class="btn btn-dark btn-lg  d-block ml-5 mt-2" data-target="#mah4" data-toggle="collapse">Reset Password</button>
							</div>
						</div>
					</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-3 mt-1">
					<div className="btn-groups">
						<button onClick={this.props.handleMain3}id="main3" class="btn btn-dark btn-lg" data-toggle="collapse" data-target="#bu4">
							<h1>Report</h1>
						</button>
						<div id="bu4" class="collapse">
							<div class="body4" id="body4">
								<button class="btn btn-warning btn-lg m-1 b-block  mt-2" data-target="#mah1" data-toggle="collapse">View</button>
							</div>
						</div>
					</div>				
			</div>
		</div>
	</div>
</div>

        )
    }
}

export default Presentation;