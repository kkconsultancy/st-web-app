import React from 'react';

class Presentation extends React.Component {
    
    render(){
        return (
          <form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
            <div className="card mt-5 mb-5">
            <div className="card-body">
              <div className="card-header text-danger">
                {this.props.error}
                {/* <img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
              </div>              
                <h4 className="card-title text-dark">USERS LIST</h4>
                <div className="row mt-3 mb-3 form-group">
                  <div className="col col-lg-8"/>
                  <div className="col col-lg-4">
                    <div className="input-group">
                      <div className="input-group-prepend">
                        <span className="input-group-text mt-5">@</span>
                      </div>
                      <input className="btn-outline-secondary btn form-control mt-5" onChange={this.props.search} type="text" placeholder="details"/>
                    </div>
                  </div>
                </div>
                <div className="row mt-3 mb-3 form-group">
                  <div className="col-12">
                    <table className="" width="100%" style={{maxHeight:"100px",overflow:"scroll"}} border="0">
                      <tbody>
                        <tr className="row">
                          <th className="col-2">ID</th>
                          <th className="col-3">NAME</th>
                          <th className="col-3">EMAIL</th>
                          <th className="col-2">MOBILE</th>
                          <th className="col-2">ROLE</th>
                        </tr>
                        {this.props.users}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </form>
        )
    }
}

export default Presentation;