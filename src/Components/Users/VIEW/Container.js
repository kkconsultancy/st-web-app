import React, {Component} from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        name : '' ,
        MobileNo : '' ,
        EmailId : '' ,
        Description : '' ,
        Address : '',
        users : ''
    }
    componentDidMount(){
        this.props.middleware.getUsers((response) => {
            if(response.code){
                return this.setState({
                    error : "FACING ISSUE WITH GETTING USERS LIST"
                })
            }
            const data = response.data
            const users = data.map(user => {
                return(
                    <tr key={user.uid} className="row">
                        <td className="text-dark col-2 border">{user.uid}</td>
                        <td className="text-dark col-3 border">{user.displayName}</td>
                        <td className="text-dark col-3 border">{user.email}</td>
                        <td className="text-dark col-2 border">{user.phoneNumber}</td>
                        <td className="text-dark col-2 border">{user.role}</td>
                    </tr>
                )
            })
            console.log(users)
            this.setState({
                users : users,
                userData : data
            })
        })
    }
    callback = (action) => {
        console.log(action)
    }
    submit = (e) => {
        e.preventDefault();
        console.log(this.state);
        const data = {
            displayName : this.state.displayName,
            phoneNumber : "+91"+this.state.phoneNumber,
            email : this.state.email
        }
        this.props.middleware.addUser(data,this.callback)
    }
    validate = (e) => {
        if(e.name==='displayName'){
            let reg=/^[A-z\s]{3,}$/
            if(!e.value.match(reg)){
                this.setState({
                    error : 'enter the correct name '
                })
                return false
            }
        }
        else if(e.name==='phoneNumber'){
            let reg=/^[0-9]{10}$/
            if(!e.value.match(reg)){
                this.setState({
                    error : 'enter the valid  MobileNo  '
                })
                return false
            }
        }
        else if(e.name==='email'){
            let reg=/^\S+@\S+$/
            if(!e.value.match(reg)){
                this.setState({
                    error : 'enter the valid EmailId@  '
                })
                return false
            }
        }
        else if(e.name==='address'){
            if(!e.value){
                this.setState({
                    error : 'enter the Address'
                })
                return false
            }
        }
        return true
    }

    updation = (e) => {
        this.setState({
            [e.target.name] : e.target.value ,
            error:''
        })
        this.validate({
            name : e.target.name,
            value : e.target.value
        })
    }

    search = (e) => {
        
        let users = []
        if(e.target.value && e.target.value.match(/.*/)){
            const query = e.target.value.toLowerCase()
            users = this.state.userData.filter(user => {
                if(user.phoneNumber.toLowerCase().match(query))
                    return true
                else if(user.displayName.toLowerCase().match(query))
                    return true
                else if(user.email.toLowerCase().match(query))
                    return true
                else if(user.role.toLowerCase().match(query))
                    return true
                return false
            }).map(user => {
                return(
                    <tr key={user.uid} className="row border">
                        <td className="text-dark col-2">{user.uid}</td>
                        <td className="text-dark col-3">{user.displayName}</td>
                        <td className="text-dark col-3">{user.email}</td>
                        <td className="text-dark col-2">{user.phoneNumber}</td>
                        <td className="text-dark col-2">{user.role}</td>
                    </tr>
                )
            })
        }
        else{
            users = this.state.userData.map(user => {
                return(
                    <tr key={user.uid} className="row border">
                        <td className="text-dark col-2">{user.uid}</td>
                        <td className="text-dark col-3">{user.displayName}</td>
                        <td className="text-dark col-3">{user.email}</td>
                        <td className="text-dark col-2">{user.phoneNumber}</td>
                        <td className="text-dark col-2">{user.role}</td>
                    </tr>
                )
            })
        }
        this.setState({
            users : users
        })
    }

    render() {

        return(
            <Presentation
            {...this.state}
            User = {this.User}
            updation = {this.updation}
            submit = {this.submit}
            search = {this.search}
            />
        );
    }
}
export default Container;