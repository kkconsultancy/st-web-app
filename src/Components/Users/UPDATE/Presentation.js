import React from 'react';


class Presentation extends React.Component {
    
    render(){
        return (
          <form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
                <div className="card0 mt-5 mb-5">
                <div className="card-body">
                    <div className="card-header text-danger">
                        {this.props.error}
                        {/* <img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    </div>                    
                        <h4 className="card-title text-dark">UPDATE EXISTING ACCOUNT</h4>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-2"/>
                            <div className="col-4 col-lg-2">
                                <label>Mobile No.</label>
                            </div>
                            <div className="col-8 col-lg-5">
                            <div class="input-group-text">+91
                              <input type="text" className="form-control" required placeholder="0123456789" onChange={this.props.updation} name="phoneNumber"/></div>
                            </div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-2"/>
                            <div className="col-4 col-lg-2">
                                <label>Name</label>
                            </div>
                            <div className="col-8 col-lg-5">
                              <input type="text" className="form-control" disabled={this.props.btnDisabled} required placeholder="Developers@Work" onChange={this.props.updation} name="displayName" value={this.props.displayName} />
                            </div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-2"/>
                            <div className="col-4 col-lg-2">
                                <label>Email</label>
                            </div>
                            <div className="col-8 col-lg-5">
                              <input type="text" className="form-control" disabled={this.props.btnDisabled} required placeholder="developerswork@developers.work" onChange={this.props.updation} name="email" value={this.props.email}/>
                            </div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-2"/>
                            <div className="col-4 col-lg-2">
                                <label>Role</label>
                            </div>
                            <div className="col-8 col-lg-5">
                            <select disabled={this.props.btnDisabled} className="form-control" onChange={this.props.updation} name="role" value={this.props.role}>
                              <option value="">---</option>
                              <option value="ADMIN">ADMIN</option>
                              <option value="BEAUTICIAN">BEAUTICIAN</option>
                              <option value="OPERATOR">OPERATOR</option>
                              <option value="USER">CUSTOMER</option>
                            </select>
                            </div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-2"/>
                            <div className="col-4 col-lg-2">
                                <label>Address</label>
                            </div>
                            <div className="col-8 col-lg-5">
                              <textarea type="text" rows="5" className="form-control" disabled={this.props.btnDisabled} required placeholder="Planet Earth" onChange={this.props.updation} name="address" value={this.props.address}/>
                            </div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-lg-4 col-2"/>
                            <div className="col-lg-2 col-4">
                                <button className="btn btn-outline-primary btn-lg active" type="submit" onClick={this.props.submit}>UPDATE</button>
                            </div>
                            <div className="col-lg-2 col-4">                
                                <button className="btn btn-outline-dark btn-lg active" type="reset" variant="primary" disabled={this.props.btnDisabled} >RESET</button>
                            </div>
                            <div className="col-lg-4 col-2"/>
                        </div>

                    </div>
              
                </div>
            </form>
        )
    }
}

export default Presentation;