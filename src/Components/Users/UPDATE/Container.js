import React, {Component} from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        displayName : '' ,
        phoneNumber : '' ,
        email : '' ,
        address : '-',
        btnDisabled : true
    }
    callback = (action) => {
        console.log(action)
        if(action.code){
            this.setState({
                error : "UPDATION FIALED",
                btnDisabled : false
            })
        }else
            this.setState({
                error : "UPDATION SUCCESSFUL",
                btnDisabled : true
            })
    }
    submit = (e) => {
        e.preventDefault();
        console.log(this.state);
        const data = {
            displayName : this.state.displayName,
            phoneNumber : this.state.phoneNumber,
            email : this.state.email,
            role : this.state.role ? this.state.role : 'USER',
            address : this.state.address ? this.state.address : '-'
        }
        this.setState({
            btnDisabled : true
        })
        this.props.middleware.updateUser(data,this.callback)
    }
    validate = (e) => {
        if(e.name==='displayName'){
            let reg=/^[A-z\s]{3,}$/
            if(!e.value.match(reg)){
                this.setState({
                    error : 'enter the correct name '
                })
                return false
            }
        }
        else if(e.name==='phoneNumber'){
            let reg=/^[0-9]{10}$/
            if(!e.value.match(reg)){
                this.setState({
                    error : 'enter the valid  MobileNo  '
                })
                return false
            }else{
                this.props.middleware.getUid(e.value,(response)=>{
                    if(response.code){
                        this.setState({
                            displayName : "",
                            email : "",
                            btnDisabled : true,
                            address : "",
                            role : "",
                            error : "USER ID NOT FOUND"
                        })
                    }
                    else{
                        const data = response.data
                        // console.log(data)
                        this.setState({
                            displayName : data.displayName,
                            email : data.email,
                            btnDisabled : false,
                            address : data.address,
                            role : data.role
                        })
                    }
                })
            }
        }
        else if(e.name==='email'){
            let reg=/^\S+@\S+$/
            if(!e.value.match(reg)){
                this.setState({
                    error : 'enter the valid EmailId@  '
                })
                return false
            }
        }
        else if(e.name === 'address'){
            if(!e.value){
                this.setState({
                    error : 'enter the Address'
                })
                return false
            }
        }
        return true
    }

    updation = (e) => {
        this.setState({
            [e.target.name] : e.target.value ,
            error:''
        })
        this.validate({
            name : e.target.name,
            value : e.target.value
        })
    }

    render() {

        return(
            <Presentation
            {...this.state}
            User = {this.User}
            updation = {this.updation}
            submit = {this.submit}
            />
        );
    }
}
export default Container;