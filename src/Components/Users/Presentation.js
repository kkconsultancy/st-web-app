import React from 'react';
import {Route,Redirect,Switch} from 'react-router-dom'
// import GoogleAd from '../GoogleAd'

import ADD from './ADD'
import VIEW from './VIEW'
import UPDATE from './UPDATE'
import RESETPASSWORD from './RESET PASSWORD'

class Presentation extends React.Component {

    render(){
        let action = this.props.middleware.state.user.services
        // console.log(action)
        for(let i in action)
            if(i === "USERS")
        return (
              <div>
                  <Switch>
                    <Route path="/users/add" component={() => <ADD middleware={this.props.middleware}/>}/>
                    <Route path="/users/view" component={() => <VIEW middleware={this.props.middleware}/>}/>
                    <Route path="/users/update" component={() => <UPDATE middleware={this.props.middleware}/>}/>
                    <Route path="/users/reset-password" component={() => <RESETPASSWORD middleware={this.props.middleware}/>}/>
                    <Redirect from="**" to="/users/add"/>
                </Switch>
              </div>
        )
        return (
            <h1>404</h1>
        )
    }
}

export default Presentation;