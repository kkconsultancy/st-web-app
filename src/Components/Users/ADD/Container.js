import React, {Component} from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        displayName : '' ,
        phoneNumber : '' ,
        email : '' ,
        error : '',
        address : ''
    }
    componentDidMount = () => {
        this.props.middleware.getUsers((response)=>{
            console.log(response)
        })
    }
    callback = (action) => {
        console.log(action)
        if(action.code){
            this.setState({
                error : action.message
            })
        }else{
            this.setState({
                error : "USER REGISTRATION COMPLETED"
            })
        }
    }
    submit = (e) => {
        e.preventDefault();
        if(this.state.error)
            return;
        for(let i in this.state){
            if(!this.validate({
                target : {
                    name : i,
                    value : this.state[i]
                }
            })) return;
        }
        console.log(this.state);
        const data = {
            displayName : this.state.displayName,
            phoneNumber : this.state.phoneNumber,
            email : this.state.email,
            address : this.state.address
        }
        this.props.middleware.addUser(data,this.callback)
    }
    validate = (e) => {
        // console.log(this.state)
        if(e.target.name==='displayName'){
            let reg=/^[A-z\s]{3,}$/
            if(!e.target.value.match(reg)){
                this.setState({
                    error : 'NAME OF THE USER IS INVALID'
                })
                return false
            }
        }
        else if(e.target.name==='phoneNumber'){
            let reg=/^[0-9]{10}$/
            // console.log(e.target.value.match(reg))
            if(!e.target.value.match(reg)){
                this.setState({
                    error : "MOBILE NO. IS INVALID"
                })
                return false
            }

            this.props.middleware.getUsers((response) => {
                const users = response.data
                for(let i in users){
                    if(users[i].uid === e.value || users[i].phoneNumber === e.value){
                        this.setState({
                            error : "USER WITH PROVIDED MOBILE NO. ALREADY EXISTS"
                        })
                        return false
                    }
                }
            })
        }
        else if(e.target.name==='email'){
            let reg=/^[A-z0-9_.-]+@[A-z]+\..+$/
            if(!e.target.value.match(reg)){
                this.setState({
                    error : 'INVALID EMIAL ID'
                })
                return false
            }
        }
        return true
    }

    updation = (e) => {
        this.setState({
            [e.target.name] : e.target.value ,
            error:''
        })
        this.validate({
            target:{
                name : e.target.name,
                value : e.target.value
            }
        })
    }

    render() {

        return(
            <Presentation
            {...this.state}
            User = {this.User}
            updation = {this.updation}
            submit = {this.submit}
            />
        );
    }
}
export default Container;