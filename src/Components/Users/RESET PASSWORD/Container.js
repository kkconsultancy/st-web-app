import React, {Component} from 'react';
import Presentation from './Presentation';
class Container extends Component{
    state = {
        name : '' ,
        MobileNo : '' ,
        EmailId : '' ,
        Description : '' ,
        Address : '',
        users : ''
    }
    componentDidMount(){
    }
    callback = (action) => {
        console.log(action)
    }
    submit = (e) => {
        e.preventDefault();
        console.log(this.state);
        const data = {
            username : this.state.username
        }
        this.props.middleware.resetPassword(data,(response)=>{
            console.log(response)
        })
    }
    validate = (e) => {
        if(e.target.name === "username"){
            e.target.value = "+91" + e.target.value
            this.setState({
                username : e.target.value
            })
            if(!e.target.value.match(/^\+[0-9]{12}$/)){
                return this.setState({
                    error : "INVALID MOBILE NUMBER",
                    parents : ''
                })
            }else{
                this.props.middleware.getUid(e.target.value,(response) => {
                    if(!response.code){
                        const data = response.data
                        this.setState({
                            customerInfo : (
                                <div>
                                    <div>Name : {data.displayName}</div>
                                    <div>Address : {data.address}</div>
                                    <div>Email : {data.email}</div>
                                </div>
                            ),
                            user : response.data,
                            error : ''
                        })
                    }else
                        this.setState({
                            error : "USER NOT FOUND"
                        })
                })
            }
        }
        return true
    }

    updation = (e) => {
        this.setState({
            [e.target.name] : e.target.value ,
            error:'',
            customerInfo : '',
            user : ''
        })
        this.validate({
            target : {
                name : e.target.name,
                value : e.target.value
            }
        })
    }

    render() {

        return(
            <Presentation
            {...this.state}
            User = {this.User}
            update = {this.updation}
            submit = {this.submit}
            />
        );
    }
}
export default Container;