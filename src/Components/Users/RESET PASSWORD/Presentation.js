import React from 'react';

class Presentation extends React.Component {
    
    render(){
        return (
          <form onSubmit={this.props.submit} className="mt-5 mb-5 container text-center">
                <div className="card0 mt-5 mb-5">
                <div className="card-body">
                    <div className="card-header text-danger">
                        {this.props.error}
                        {/* <img className="card-img-top" src="https://developerswork.online/wp-content/uploads/2019/02/ChannelArt.png" alt="Card image"></img> */}
                    </div>
                    
                        <h4 className="card-title text-dark">RESET PASSWORD</h4>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-2"/>
                            <div className="col-4 col-lg-2">
                                <label>Mobile No.</label>
                            </div>
                            
                            <div className="col-8 col-lg-4 input-group">
					<div class="input-group-text">
						<div class="input-group-text">+91</div>
						<input type="text"  className="form-control" name="username" placeholder="9999999990" onChange={this.props.update} />
					</div>
				</div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-0 col-lg-3"/>
                            <div className="col-8 col-lg-6 border text-left">
                              {this.props.customerInfo}
                            </div>
                            <div className="col-0 col-lg-3"/>
                        </div>
                        <div className="row mt-3 mb-3 form-group">
                            <div className="col-4"/>
                            <div className="col-4">
                                <button className="btn btn-danger" type="submit" onClick={this.props.submit}>RESET PASSWORD</button>
                            </div>
                            <div className="col-4"/>
                        </div>

                    </div>
              
                </div>
            </form>
        )
    }
}

export default Presentation;